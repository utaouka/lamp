﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//=============================================================//
/**
 * @brief メイン処理
 * @since2014.04.26
 * @author 大阪電気通信大学総合情報学部デジタルゲーム学科
 * 		   HW11A020 大野哲平.
*/
public class AppMain : MonoBehaviour {

	#region メンバー変数
	//=============================================================//
	/**
	 * @brief シーンタスクのインスタンス
	*/
	public static AppSceneMainTask AppSceneMainTask;
	/**
	 * @brief オブジェクトタスク
	*/
	public static ManageObjectTask NowLoadingObjectTask;
	/**	
	 * @brief ポーズフラグ
	*/
	private static bool m_f_pause = false;
	//=============================================================//
	#endregion

	//シーン開始時に一回だけ読み込む
	public virtual void Start ()
	{
		AppSceneMainTask.Initialize ();
		AppObjectMainTask.AddObjectTask ();
		NowLoadingObjectTask.OBJ_TASK.Initialize ();

		for (int i = 1; i < AppObjectMainTask.GetObjectTaskCount (); i++)
		{
			AppObjectMainTask.ChangeObjectTask ( NowLoadingObjectTask );
			NowLoadingObjectTask.OBJ_TASK.Initialize ();
		}
		AppObjectMainTask.ChangeObjectTask ( NowLoadingObjectTask );
	}

	//実行処理
	public virtual void Update ()
	{
		if (NowLoadingObjectTask == null) 
		{
			return;
		}
		//TODO:シーン切り替え時に処理を一時的に止める
		if (!m_f_pause)
		{
			AppSceneMainTask.Execution ();
			TouchManager.Execution ();
			NowLoadingObjectTask.OBJ_TASK.Execution ();
			for (int i = 1; i < AppObjectMainTask.GetObjectTaskCount (); i++) 
			{
				AppObjectMainTask.ChangeObjectTask (NowLoadingObjectTask);
				NowLoadingObjectTask.OBJ_TASK.Execution ();
			}
			//オブジェクトタスクの削除
			AppObjectMainTask.RemoveTask ();
			//オブジェクトタスクの追加
			AppObjectMainTask.AddObjectTask ();
			//現在読み込まれているオブジェクトタスクを初期に戻す
			AppObjectMainTask.ChangeObjectTask (NowLoadingObjectTask);
		}
		else 
		{
			AppSceneMainTask.PauseExecution ();
			TouchManager.PauseExecution ();
			NowLoadingObjectTask.OBJ_TASK.PauseExecution ();
			for (int i = 1; i < AppObjectMainTask.GetObjectTaskCount (); i++) 
			{
				AppObjectMainTask.ChangeObjectTask (NowLoadingObjectTask);
				NowLoadingObjectTask.OBJ_TASK.PauseExecution ();
			}
			AppObjectMainTask.ChangeObjectTask (NowLoadingObjectTask);
		}
	}
	//終了処理
	void OnApplicationQuit()
	{
		AppSceneMainTask.Terminate ();
		if (NowLoadingObjectTask != null)
		{
			NowLoadingObjectTask.OBJ_TASK.Terminate ();
			for (int i = 1; i < AppObjectMainTask.GetObjectTaskCount (); i++) 
			{
				AppObjectMainTask.ChangeObjectTask (NowLoadingObjectTask);
				NowLoadingObjectTask.OBJ_TASK.Terminate ();
			}
			NowLoadingObjectTask = null;
		}
		AppObjectMainTask.TaskClearAll();

	}
	//=============================================================//
	/**	
	 * @brief ポーズフラグの設定
	*/
	public static void SetPauseFlag( bool pause )
	{
		m_f_pause = pause;
	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief ポーズフラグの取得
	*/
	public static bool GetPauseFlag()
	{
		return m_f_pause;
	}
	//=============================================================//
}
