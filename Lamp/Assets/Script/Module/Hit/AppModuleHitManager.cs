﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//=============================================================//
/**
 * @brief ヒットマネージャー
*/
public class AppModuleHitManager {

    //=============================================================//
    /**
     * @brief プレイヤーと接触しているかどうか
     * @param Transform オブジェクト
     * @return bool
    */
	public bool isHitPlayer(Transform obj_transform)
	{
		Vector3 player_position = AppScenePlaySceneTask.GetPlayerInstance().GetlocalPosition();
		Vector3 player_scale = AppScenePlaySceneTask.GetPlayerInstance().GetScale();
		
		Vector3 player_left_down_position = player_position - player_scale / 2f;
		Vector3 player_right_up_position = player_position + player_scale / 2f;
		
		Vector3 wall_left_down_position = obj_transform.localPosition - obj_transform.localScale / 2f;
		Vector3 wall_right_up_position = obj_transform.localPosition + obj_transform.localScale / 2f;
		
		if(player_left_down_position.x <= wall_right_up_position.x
		 && wall_left_down_position.x <= player_right_up_position.x
		 && player_left_down_position.y <= wall_right_up_position.y
		 && wall_left_down_position.y <= player_right_up_position.y)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
    //=============================================================//
	
    //=============================================================//
    /**
     * @brief プレイヤーと障害物の接触方向
     * @param Transform オブジェクト
     * @param Vector3 obj_addPos
     * @param Vector3 player_addPos
     * @return Vector3
    */
    public Vector3 CheckHitVector(Transform obj_transform, Vector3 obj_addPos, Vector3 player_addPos)
	{
        Vector3 player_pre_position = AppScenePlaySceneTask.GetPlayerInstance().GetPrelocalPosition(3);
        Vector3 player_position = AppScenePlaySceneTask.GetPlayerInstance().GetlocalPosition();
		Vector3 player_scale = AppScenePlaySceneTask.GetPlayerInstance().GetScale();
		
        Vector3[] player_pre_apex = new Vector3[4];
        player_pre_apex[0] = new Vector3(player_pre_position.x - (player_scale.x / 2f), player_pre_position.y + (player_scale.y / 2f));
        player_pre_apex[1] = new Vector3(player_pre_position.x + (player_scale.x / 2f), player_pre_position.y + (player_scale.y / 2f));
        player_pre_apex[2] = new Vector3(player_pre_position.x + (player_scale.x / 2f), player_pre_position.y - (player_scale.y / 2f));
        player_pre_apex[3] = new Vector3(player_pre_position.x - (player_scale.x / 2f), player_pre_position.y - (player_scale.y / 2f));

        Vector3[] player_apex = new Vector3[4];
        player_apex[0] = new Vector3(player_position.x - (player_scale.x / 2f), player_position.y + (player_scale.y / 2f)) + player_addPos;
        player_apex[1] = new Vector3(player_position.x + (player_scale.x / 2f), player_position.y + (player_scale.y / 2f)) + player_addPos;
        player_apex[2] = new Vector3(player_position.x + (player_scale.x / 2f), player_position.y - (player_scale.y / 2f)) + player_addPos;
        player_apex[3] = new Vector3(player_position.x - (player_scale.x / 2f), player_position.y - (player_scale.y / 2f)) + player_addPos;

        Vector3 obj_position = obj_transform.localPosition + obj_addPos;

        Vector3[] def = CalculateApex(obj_position, obj_transform.localScale);
        float[] angle = CalculateAngle(obj_position, def);
        float[] dis = CalculateDistance(obj_position, def);
        Vector3[] addVec = CalculateAddVector(obj_transform.eulerAngles.z, angle, dis); 

        Vector3[] obj_apex = new Vector3[4];
        obj_apex[0] = obj_position + addVec[0];
        obj_apex[1] = obj_position + addVec[1];
        obj_apex[2] = obj_position + addVec[2];
        obj_apex[3] = obj_position + addVec[3];

        Vector3 vect = Vector3.zero;
        for(int player_apex_count = 0; player_apex_count < player_apex.Length; player_apex_count++)
        {
            if( ( ( (obj_apex[1] - obj_apex[0]).x * (player_apex[player_apex_count] - obj_apex[0]).y ) - ((obj_apex[1] - obj_apex[0]).y * (player_apex[player_apex_count] - obj_apex[0]).x) < 0 ) &&
                ( ( (obj_apex[3] - obj_apex[2]).x * (player_apex[player_apex_count] - obj_apex[2]).y ) - ((obj_apex[3] - obj_apex[2]).y * (player_apex[player_apex_count] - obj_apex[2]).x) < 0 ) &&
                ( ( (obj_apex[2] - obj_apex[1]).x * (player_apex[player_apex_count] - obj_apex[1]).y ) - ((obj_apex[2] - obj_apex[1]).y * (player_apex[player_apex_count] - obj_apex[1]).x) < 0 ) &&
                ( ( (obj_apex[0] - obj_apex[3]).x * (player_apex[player_apex_count] - obj_apex[3]).y ) - ((obj_apex[0] - obj_apex[3]).y * (player_apex[player_apex_count] - obj_apex[3]).x) < 0 ) )
            {
                //どの直線にぶつかったか調べる
                if (((player_pre_apex[player_apex_count].x - player_apex[player_apex_count].x) * (obj_apex[0].y - player_pre_apex[player_apex_count].y) + (player_pre_apex[player_apex_count].y - player_apex[player_apex_count].y) * (player_pre_apex[player_apex_count].x - obj_apex[0].x)) *
                    ((player_pre_apex[player_apex_count].x - player_apex[player_apex_count].x) * (obj_apex[1].y - player_pre_apex[player_apex_count].y) + (player_pre_apex[player_apex_count].y - player_apex[player_apex_count].y) * (player_pre_apex[player_apex_count].x - obj_apex[1].x)) < 0)
                {
                    if (((obj_apex[0].x - obj_apex[1].x) * (player_pre_apex[player_apex_count].y - obj_apex[0].y) + (obj_apex[0].y - obj_apex[1].y) * (obj_apex[0].x - player_pre_apex[player_apex_count].x)) *
                        ((obj_apex[0].x - obj_apex[1].x) * (player_apex[player_apex_count].y - obj_apex[0].y) + (obj_apex[0].y - obj_apex[1].y) * (obj_apex[0].x - player_apex[player_apex_count].x)) < 0)
                    {
                        if( ( ( obj_apex[1] - obj_apex[0] ).x * ((player_position + player_addPos) - obj_apex[0]).y ) - ((obj_apex[1] - obj_apex[0]).y * ((player_position + player_addPos) - obj_apex[0]).x ) > 0 )
                        {
                            vect = (obj_apex[1] - obj_apex[0]);
                            break;
                        }
                    }
                }
                if (((player_pre_apex[player_apex_count].x - player_apex[player_apex_count].x) * (obj_apex[2].y - player_pre_apex[player_apex_count].y) + (player_pre_apex[player_apex_count].y - player_apex[player_apex_count].y) * (player_pre_apex[player_apex_count].x - obj_apex[2].x)) *
                    ((player_pre_apex[player_apex_count].x - player_apex[player_apex_count].x) * (obj_apex[3].y - player_pre_apex[player_apex_count].y) + (player_pre_apex[player_apex_count].y - player_apex[player_apex_count].y) * (player_pre_apex[player_apex_count].x - obj_apex[3].x)) < 0)
                {
                    if (((obj_apex[2].x - obj_apex[3].x) * (player_pre_apex[player_apex_count].y - obj_apex[2].y) + (obj_apex[2].y - obj_apex[3].y) * (obj_apex[2].x - player_pre_apex[player_apex_count].x)) *
                        ((obj_apex[2].x - obj_apex[3].x) * (player_apex[player_apex_count].y - obj_apex[2].y) + (obj_apex[2].y - obj_apex[3].y) * (obj_apex[2].x - player_apex[player_apex_count].x)) < 0)
                    {
                        if( ( ( obj_apex[3] - obj_apex[2] ).x * ((player_position + player_addPos) - obj_apex[2]).y ) - ((obj_apex[3] - obj_apex[2]).y * ((player_position + player_addPos) - obj_apex[2]).x ) > 0 )
                        {
                            vect = (obj_apex[3] - obj_apex[2]);
                            break;
                        }
                    }
                }
                if (((player_pre_apex[player_apex_count].x - player_apex[player_apex_count].x) * (obj_apex[1].y - player_pre_apex[player_apex_count].y) + (player_pre_apex[player_apex_count].y - player_apex[player_apex_count].y) * (player_pre_apex[player_apex_count].x - obj_apex[1].x)) *
                    ((player_pre_apex[player_apex_count].x - player_apex[player_apex_count].x) * (obj_apex[2].y - player_pre_apex[player_apex_count].y) + (player_pre_apex[player_apex_count].y - player_apex[player_apex_count].y) * (player_pre_apex[player_apex_count].x - obj_apex[2].x)) < 0)
                {
                    if (((obj_apex[1].x - obj_apex[2].x) * (player_pre_apex[player_apex_count].y - obj_apex[1].y) + (obj_apex[1].y - obj_apex[2].y) * (obj_apex[1].x - player_pre_apex[player_apex_count].x)) *
                        ((obj_apex[1].x - obj_apex[2].x) * (player_apex[player_apex_count].y - obj_apex[1].y) + (obj_apex[1].y - obj_apex[2].y) * (obj_apex[1].x - player_apex[player_apex_count].x)) < 0)
                    {
                        if( ( ( obj_apex[2] - obj_apex[1] ).x * ((player_position + player_addPos) - obj_apex[1]).y ) - ((obj_apex[2] - obj_apex[1]).y * ((player_position + player_addPos) - obj_apex[1]).x ) > 0 )
                        {
                            vect = (obj_apex[2] - obj_apex[1]);
                            break;
                        }
                    }
                }
                if (((player_pre_apex[player_apex_count].x - player_apex[player_apex_count].x) * (obj_apex[3].y - player_pre_apex[player_apex_count].y) + (player_pre_apex[player_apex_count].y - player_apex[player_apex_count].y) * (player_pre_apex[player_apex_count].x - obj_apex[3].x)) *
                    ((player_pre_apex[player_apex_count].x - player_apex[player_apex_count].x) * (obj_apex[0].y - player_pre_apex[player_apex_count].y) + (player_pre_apex[player_apex_count].y - player_apex[player_apex_count].y) * (player_pre_apex[player_apex_count].x - obj_apex[0].x)) < 0)
                {
                    if (((obj_apex[3].x - obj_apex[0].x) * (player_pre_apex[player_apex_count].y - obj_apex[3].y) + (obj_apex[3].y - obj_apex[0].y) * (obj_apex[3].x - player_pre_apex[player_apex_count].x)) *
                        ((obj_apex[3].x - obj_apex[0].x) * (player_apex[player_apex_count].y - obj_apex[3].y) + (obj_apex[3].y - obj_apex[0].y) * (obj_apex[3].x - player_apex[player_apex_count].x)) < 0)
                    {
                        if( ( ( obj_apex[0] - obj_apex[3] ).x * ((player_position + player_addPos) - obj_apex[3]).y ) - ((obj_apex[0] - obj_apex[3]).y * ((player_position + player_addPos) - obj_apex[3]).x ) > 0 )
                        {
                            vect = (obj_apex[0] - obj_apex[3]);
                            break;
                        }
                    }
                }
                if(vect == Vector3.zero)
                {
                    //直交しなかった場合の対策としてやむをえず追加
                    if( ( ( obj_apex[0] - obj_position ).x * ((player_position + player_addPos) - obj_position).y ) - ((obj_apex[0] - obj_position).y * ((player_position + player_addPos) - obj_position).x ) < 0 &&
                        ( ( obj_apex[1] - obj_position ).x * ((player_position + player_addPos) - obj_position).y ) - ((obj_apex[1] - obj_position).y * ((player_position + player_addPos) - obj_position).x ) > 0 )
                    {
                        vect = (obj_apex[1] - obj_apex[0]);
                        break;
                    }
                    if( ( ( obj_apex[2] - obj_position ).x * ((player_position + player_addPos) - obj_position).y ) - ((obj_apex[2] - obj_position).y * ((player_position + player_addPos) - obj_position).x ) < 0 &&
                        ( ( obj_apex[3] - obj_position ).x * ((player_position + player_addPos) - obj_position).y ) - ((obj_apex[3] - obj_position).y * ((player_position + player_addPos) - obj_position).x ) > 0 )
                    {
                        vect = (obj_apex[3] - obj_apex[2]);
                        break;
                    }
                    if( ( ( obj_apex[1] - obj_position ).x * ((player_position + player_addPos) - obj_position).y ) - ((obj_apex[1] - obj_position).y * ((player_position + player_addPos) - obj_position).x ) < 0 &&
                        ( ( obj_apex[2] - obj_position ).x * ((player_position + player_addPos) - obj_position).y ) - ((obj_apex[2] - obj_position).y * ((player_position + player_addPos) - obj_position).x ) > 0 )
                    {
                        vect = (obj_apex[2] - obj_apex[1]);
                        break;
                    }
                    if( ( ( obj_apex[3] - obj_position ).x * ((player_position + player_addPos) - obj_position).y ) - ((obj_apex[3] - obj_position).y * ((player_position + player_addPos) - obj_position).x ) < 0 &&
                        ( ( obj_apex[0] - obj_position ).x * ((player_position + player_addPos) - obj_position).y ) - ((obj_apex[0] - obj_position).y * ((player_position + player_addPos) - obj_position).x ) > 0 )
                    {
                        vect = (obj_apex[0] - obj_apex[3]);
                        break;
                    }
                }
            }
        }
        if(vect == Vector3.zero)
        {
            for(int obj_apex_count = 0; obj_apex_count < obj_apex.Length; obj_apex_count++)
            {
                if( ( ( (player_apex[1] - player_apex[0]).x * (obj_apex[obj_apex_count] - player_apex[0]).y ) - ((player_apex[1] - player_apex[0]).y * (obj_apex[obj_apex_count] - player_apex[0]).x) < 0 ) &&
                    ( ( (player_apex[3] - player_apex[2]).x * (obj_apex[obj_apex_count] - player_apex[2]).y ) - ((player_apex[3] - player_apex[2]).y * (obj_apex[obj_apex_count] - player_apex[2]).x) < 0 ) &&
                    ( ( (player_apex[2] - player_apex[1]).x * (obj_apex[obj_apex_count] - player_apex[1]).y ) - ((player_apex[2] - player_apex[1]).y * (obj_apex[obj_apex_count] - player_apex[1]).x) < 0 ) &&
                    ( ( (player_apex[0] - player_apex[3]).x * (obj_apex[obj_apex_count] - player_apex[3]).y ) - ((player_apex[0] - player_apex[3]).y * (obj_apex[obj_apex_count] - player_apex[3]).x) < 0 ) )
                {
                    //プレイヤーから壁に対する当たり判定(簡易)
                    if( ( ( player_apex[0] - player_position ).x * (obj_position - player_position).y ) - ((player_apex[0] - player_position).y * (obj_position - player_position).x ) < 0 &&
                        ( ( player_apex[1] - player_position ).x * (obj_position - player_position).y ) - ((player_apex[1] - player_position).y * (obj_position - player_position).x ) > 0 )
                    {
                        vect = (player_apex[0] - player_apex[1]);
                        break;
                    }
                    if( ( ( player_apex[2] - player_position ).x * (obj_position - player_position).y ) - ((player_apex[2] - player_position).y * (obj_position - player_position).x ) < 0 &&
                        ( ( player_apex[3] - player_position ).x * (obj_position - player_position).y ) - ((player_apex[3] - player_position).y * (obj_position - player_position).x ) > 0 )
                    {
                        vect = (player_apex[2] - player_apex[3]);
                        break;
                    }
                    if( ( ( player_apex[1] - player_position ).x * (obj_position - player_position).y ) - ((player_apex[1] - player_position).y * (obj_position - player_position).x ) < 0 &&
                        ( ( player_apex[2] - player_position ).x * (obj_position - player_position).y ) - ((player_apex[2] - player_position).y * (obj_position - player_position).x ) > 0 )
                    {
                        vect = (player_apex[1] - player_apex[2]);
                        break;
                    }
                    if( ( ( player_apex[3] - player_position ).x * (obj_position - player_position).y ) - ((player_apex[3] - player_position).y * (obj_position - player_position).x ) < 0 &&
                        ( ( player_apex[0] - player_position ).x * (obj_position - player_position).y ) - ((player_apex[0] - player_position).y * (obj_position - player_position).x ) > 0 )
                    {
                        vect = (player_apex[3] - player_apex[0]);
                        break;
                    }
                }
            }
        }
        return vect;
    }
    //=============================================================//

    //=============================================================//
    /**
     * @brief エネミーと障害物の接触方向
     * @param Transform オブジェクト
     * @param Vector3 obj_addPos,
     * @param Transform エネミー
     * @param Vector3 obj2_addPos
     * @return Vector3
    */
    public Vector3 CheckHitVector2(Transform obj_transform, Vector3 obj_addPos, Transform enemy_transform, Vector3 enemy_addPos)
    {
        Vector3 enemy_position = enemy_transform.localPosition;
        Vector3 enemy_scale = enemy_transform.localScale;
        Vector3[] enemy_pre_apex = new Vector3[4];

        enemy_pre_apex[0] = new Vector3(enemy_position.x - (enemy_scale.x / 2f), enemy_position.y + (enemy_scale.y / 2f));
        enemy_pre_apex[1] = new Vector3(enemy_position.x + (enemy_scale.x / 2f), enemy_position.y + (enemy_scale.y / 2f));
        enemy_pre_apex[2] = new Vector3(enemy_position.x + (enemy_scale.x / 2f), enemy_position.y - (enemy_scale.y / 2f));
        enemy_pre_apex[3] = new Vector3(enemy_position.x - (enemy_scale.x / 2f), enemy_position.y - (enemy_scale.y / 2f));

        Vector3[] enemy_apex = new Vector3[4];
        enemy_apex[0] = new Vector3(enemy_position.x - (enemy_scale.x / 2f), enemy_position.y + (enemy_scale.y / 2f)) + enemy_addPos;
        enemy_apex[1] = new Vector3(enemy_position.x + (enemy_scale.x / 2f), enemy_position.y + (enemy_scale.y / 2f)) + enemy_addPos;
        enemy_apex[2] = new Vector3(enemy_position.x + (enemy_scale.x / 2f), enemy_position.y - (enemy_scale.y / 2f)) + enemy_addPos;
        enemy_apex[3] = new Vector3(enemy_position.x - (enemy_scale.x / 2f), enemy_position.y - (enemy_scale.y / 2f)) + enemy_addPos;

        Vector3 obj_position = obj_transform.localPosition + obj_addPos;

        Vector3[] def = CalculateApex(obj_position, obj_transform.localScale);
        float[] angle = CalculateAngle(obj_position, def);
        float[] dis = CalculateDistance(obj_position, def);
        Vector3[] addVec = CalculateAddVector(obj_transform.eulerAngles.z, angle, dis); 

        Vector3[] obj_apex = new Vector3[4];
        obj_apex[0] = obj_position + addVec[0];
        obj_apex[1] = obj_position + addVec[1];
        obj_apex[2] = obj_position + addVec[2];
        obj_apex[3] = obj_position + addVec[3];

        Vector3 vect = Vector3.zero;
        for(int enemy_apex_count = 0; enemy_apex_count < enemy_apex.Length; enemy_apex_count++)
        {
            if( ( ( (obj_apex[1] - obj_apex[0]).x * (enemy_apex[enemy_apex_count] - obj_apex[0]).y ) - ((obj_apex[1] - obj_apex[0]).y * (enemy_apex[enemy_apex_count] - obj_apex[0]).x) < 0 ) &&
                ( ( (obj_apex[3] - obj_apex[2]).x * (enemy_apex[enemy_apex_count] - obj_apex[2]).y ) - ((obj_apex[3] - obj_apex[2]).y * (enemy_apex[enemy_apex_count] - obj_apex[2]).x) < 0 ) &&
                ( ( (obj_apex[2] - obj_apex[1]).x * (enemy_apex[enemy_apex_count] - obj_apex[1]).y ) - ((obj_apex[2] - obj_apex[1]).y * (enemy_apex[enemy_apex_count] - obj_apex[1]).x) < 0 ) &&
                ( ( (obj_apex[0] - obj_apex[3]).x * (enemy_apex[enemy_apex_count] - obj_apex[3]).y ) - ((obj_apex[0] - obj_apex[3]).y * (enemy_apex[enemy_apex_count] - obj_apex[3]).x) < 0 ) )
            {
                //どの直線にぶつかったか調べる
                if (((enemy_pre_apex[enemy_apex_count].x - enemy_apex[enemy_apex_count].x) * (obj_apex[0].y - enemy_pre_apex[enemy_apex_count].y) + (enemy_pre_apex[enemy_apex_count].y - enemy_apex[enemy_apex_count].y) * (enemy_pre_apex[enemy_apex_count].x - obj_apex[0].x)) *
                    ((enemy_pre_apex[enemy_apex_count].x - enemy_apex[enemy_apex_count].x) * (obj_apex[1].y - enemy_pre_apex[enemy_apex_count].y) + (enemy_pre_apex[enemy_apex_count].y - enemy_apex[enemy_apex_count].y) * (enemy_pre_apex[enemy_apex_count].x - obj_apex[1].x)) < 0)
                {
                    if (((obj_apex[0].x - obj_apex[1].x) * (enemy_pre_apex[enemy_apex_count].y - obj_apex[0].y) + (obj_apex[0].y - obj_apex[1].y) * (obj_apex[0].x - enemy_pre_apex[enemy_apex_count].x)) *
                        ((obj_apex[0].x - obj_apex[1].x) * (enemy_apex[enemy_apex_count].y - obj_apex[0].y) + (obj_apex[0].y - obj_apex[1].y) * (obj_apex[0].x - enemy_apex[enemy_apex_count].x)) < 0)
                    {
                        if( ( ( obj_apex[1] - obj_apex[0] ).x * ((enemy_position + enemy_addPos) - obj_apex[0]).y ) - ((obj_apex[1] - obj_apex[0]).y * ((enemy_position + enemy_addPos) - obj_apex[0]).x ) > 0 )
                        {
                            vect = (obj_apex[1] - obj_apex[0]);
                            break;
                        }
                    }
                }
                if (((enemy_pre_apex[enemy_apex_count].x - enemy_apex[enemy_apex_count].x) * (obj_apex[2].y - enemy_pre_apex[enemy_apex_count].y) + (enemy_pre_apex[enemy_apex_count].y - enemy_apex[enemy_apex_count].y) * (enemy_pre_apex[enemy_apex_count].x - obj_apex[2].x)) *
                    ((enemy_pre_apex[enemy_apex_count].x - enemy_apex[enemy_apex_count].x) * (obj_apex[3].y - enemy_pre_apex[enemy_apex_count].y) + (enemy_pre_apex[enemy_apex_count].y - enemy_apex[enemy_apex_count].y) * (enemy_pre_apex[enemy_apex_count].x - obj_apex[3].x)) < 0)
                {
                    if (((obj_apex[2].x - obj_apex[3].x) * (enemy_pre_apex[enemy_apex_count].y - obj_apex[2].y) + (obj_apex[2].y - obj_apex[3].y) * (obj_apex[2].x - enemy_pre_apex[enemy_apex_count].x)) *
                        ((obj_apex[2].x - obj_apex[3].x) * (enemy_apex[enemy_apex_count].y - obj_apex[2].y) + (obj_apex[2].y - obj_apex[3].y) * (obj_apex[2].x - enemy_apex[enemy_apex_count].x)) < 0)
                    {
                        if( ( ( obj_apex[3] - obj_apex[2] ).x * ((enemy_position + enemy_addPos) - obj_apex[2]).y ) - ((obj_apex[3] - obj_apex[2]).y * ((enemy_position + enemy_addPos) - obj_apex[2]).x ) > 0 )
                        {
                            vect = (obj_apex[3] - obj_apex[2]);
                            break;
                        }
                    }
                }
                if (((enemy_pre_apex[enemy_apex_count].x - enemy_apex[enemy_apex_count].x) * (obj_apex[1].y - enemy_pre_apex[enemy_apex_count].y) + (enemy_pre_apex[enemy_apex_count].y - enemy_apex[enemy_apex_count].y) * (enemy_pre_apex[enemy_apex_count].x - obj_apex[1].x)) *
                    ((enemy_pre_apex[enemy_apex_count].x - enemy_apex[enemy_apex_count].x) * (obj_apex[2].y - enemy_pre_apex[enemy_apex_count].y) + (enemy_pre_apex[enemy_apex_count].y - enemy_apex[enemy_apex_count].y) * (enemy_pre_apex[enemy_apex_count].x - obj_apex[2].x)) < 0)
                {
                    if (((obj_apex[1].x - obj_apex[2].x) * (enemy_pre_apex[enemy_apex_count].y - obj_apex[1].y) + (obj_apex[1].y - obj_apex[2].y) * (obj_apex[1].x - enemy_pre_apex[enemy_apex_count].x)) *
                        ((obj_apex[1].x - obj_apex[2].x) * (enemy_apex[enemy_apex_count].y - obj_apex[1].y) + (obj_apex[1].y - obj_apex[2].y) * (obj_apex[1].x - enemy_apex[enemy_apex_count].x)) < 0)
                    {
                        if( ( ( obj_apex[2] - obj_apex[1] ).x * ((enemy_position + enemy_addPos) - obj_apex[1]).y ) - ((obj_apex[2] - obj_apex[1]).y * ((enemy_position + enemy_addPos) - obj_apex[1]).x ) > 0 )
                        {
                            vect = (obj_apex[2] - obj_apex[1]);
                            break;
                        }
                    }
                }
                if (((enemy_pre_apex[enemy_apex_count].x - enemy_apex[enemy_apex_count].x) * (obj_apex[3].y - enemy_pre_apex[enemy_apex_count].y) + (enemy_pre_apex[enemy_apex_count].y - enemy_apex[enemy_apex_count].y) * (enemy_pre_apex[enemy_apex_count].x - obj_apex[3].x)) *
                    ((enemy_pre_apex[enemy_apex_count].x - enemy_apex[enemy_apex_count].x) * (obj_apex[0].y - enemy_pre_apex[enemy_apex_count].y) + (enemy_pre_apex[enemy_apex_count].y - enemy_apex[enemy_apex_count].y) * (enemy_pre_apex[enemy_apex_count].x - obj_apex[0].x)) < 0)
                {
                    if (((obj_apex[3].x - obj_apex[0].x) * (enemy_pre_apex[enemy_apex_count].y - obj_apex[3].y) + (obj_apex[3].y - obj_apex[0].y) * (obj_apex[3].x - enemy_pre_apex[enemy_apex_count].x)) *
                        ((obj_apex[3].x - obj_apex[0].x) * (enemy_apex[enemy_apex_count].y - obj_apex[3].y) + (obj_apex[3].y - obj_apex[0].y) * (obj_apex[3].x - enemy_apex[enemy_apex_count].x)) < 0)
                    {
                        if( ( ( obj_apex[0] - obj_apex[3] ).x * ((enemy_position + enemy_addPos) - obj_apex[3]).y ) - ((obj_apex[0] - obj_apex[3]).y * ((enemy_position + enemy_addPos) - obj_apex[3]).x ) > 0 )
                        {
                            vect = (obj_apex[0] - obj_apex[3]);
                            break;
                        }
                    }
                }
                if(vect == Vector3.zero)
                {
                    //直交しなかった場合の対策としてやむをえず追加
                    if( ( ( obj_apex[0] - obj_position ).x * ((enemy_position + enemy_addPos) - obj_position).y ) - ((obj_apex[0] - obj_position).y * ((enemy_position + enemy_addPos) - obj_position).x ) < 0 &&
                        ( ( obj_apex[1] - obj_position ).x * ((enemy_position + enemy_addPos) - obj_position).y ) - ((obj_apex[1] - obj_position).y * ((enemy_position + enemy_addPos) - obj_position).x ) > 0 )
                    {
                        vect = (obj_apex[1] - obj_apex[0]);
                        break;
                    }
                    if( ( ( obj_apex[2] - obj_position ).x * ((enemy_position + enemy_addPos) - obj_position).y ) - ((obj_apex[2] - obj_position).y * ((enemy_position + enemy_addPos) - obj_position).x ) < 0 &&
                        ( ( obj_apex[3] - obj_position ).x * ((enemy_position + enemy_addPos) - obj_position).y ) - ((obj_apex[3] - obj_position).y * ((enemy_position + enemy_addPos) - obj_position).x ) > 0 )
                    {
                        vect = (obj_apex[3] - obj_apex[2]);
                        break;
                    }
                    if( ( ( obj_apex[1] - obj_position ).x * ((enemy_position + enemy_addPos) - obj_position).y ) - ((obj_apex[1] - obj_position).y * ((enemy_position + enemy_addPos) - obj_position).x ) < 0 &&
                        ( ( obj_apex[2] - obj_position ).x * ((enemy_position + enemy_addPos) - obj_position).y ) - ((obj_apex[2] - obj_position).y * ((enemy_position + enemy_addPos) - obj_position).x ) > 0 )
                    {
                        vect = (obj_apex[2] - obj_apex[1]);
                        break;
                    }
                    if( ( ( obj_apex[3] - obj_position ).x * ((enemy_position + enemy_addPos) - obj_position).y ) - ((obj_apex[3] - obj_position).y * ((enemy_position + enemy_addPos) - obj_position).x ) < 0 &&
                        ( ( obj_apex[0] - obj_position ).x * ((enemy_position + enemy_addPos) - obj_position).y ) - ((obj_apex[0] - obj_position).y * ((enemy_position + enemy_addPos) - obj_position).x ) > 0 )
                    {
                        vect = (obj_apex[0] - obj_apex[3]);
                        break;
                    }
                }
            }
        }
        if(vect == Vector3.zero)
        {
            for(int obj_apex_count = 0; obj_apex_count < obj_apex.Length; obj_apex_count++)
            {
                if( ( ( (enemy_apex[1] - enemy_apex[0]).x * (obj_apex[obj_apex_count] - enemy_apex[0]).y ) - ((enemy_apex[1] - enemy_apex[0]).y * (obj_apex[obj_apex_count] - enemy_apex[0]).x) < 0 ) &&
                    ( ( (enemy_apex[3] - enemy_apex[2]).x * (obj_apex[obj_apex_count] - enemy_apex[2]).y ) - ((enemy_apex[3] - enemy_apex[2]).y * (obj_apex[obj_apex_count] - enemy_apex[2]).x) < 0 ) &&
                    ( ( (enemy_apex[2] - enemy_apex[1]).x * (obj_apex[obj_apex_count] - enemy_apex[1]).y ) - ((enemy_apex[2] - enemy_apex[1]).y * (obj_apex[obj_apex_count] - enemy_apex[1]).x) < 0 ) &&
                    ( ( (enemy_apex[0] - enemy_apex[3]).x * (obj_apex[obj_apex_count] - enemy_apex[3]).y ) - ((enemy_apex[0] - enemy_apex[3]).y * (obj_apex[obj_apex_count] - enemy_apex[3]).x) < 0 ) )
                {
                    //敵から壁に対する当たり判定(簡易)
                    if( ( ( enemy_apex[0] - enemy_position ).x * (obj_position - enemy_position).y ) - ((enemy_apex[0] - enemy_position).y * (obj_position - enemy_position).x ) < 0 &&
                        ( ( enemy_apex[1] - enemy_position ).x * (obj_position - enemy_position).y ) - ((enemy_apex[1] - enemy_position).y * (obj_position - enemy_position).x ) > 0 )
                    {
                        vect = (enemy_apex[0] - enemy_apex[1]);
                        break;
                    }
                    if( ( ( enemy_apex[2] - enemy_position ).x * (obj_position - enemy_position).y ) - ((enemy_apex[2] - enemy_position).y * (obj_position - enemy_position).x ) < 0 &&
                        ( ( enemy_apex[3] - enemy_position ).x * (obj_position - enemy_position).y ) - ((enemy_apex[3] - enemy_position).y * (obj_position - enemy_position).x ) > 0 )
                    {
                        vect = (enemy_apex[2] - enemy_apex[3]);
                        break;
                    }
                    if( ( ( enemy_apex[1] - enemy_position ).x * (obj_position - enemy_position).y ) - ((enemy_apex[1] - enemy_position).y * (obj_position - enemy_position).x ) < 0 &&
                        ( ( enemy_apex[2] - enemy_position ).x * (obj_position - enemy_position).y ) - ((enemy_apex[2] - enemy_position).y * (obj_position - enemy_position).x ) > 0 )
                    {
                        vect = (enemy_apex[1] - enemy_apex[2]);
                        break;
                    }
                    if( ( ( enemy_apex[3] - enemy_position ).x * (obj_position - enemy_position).y ) - ((enemy_apex[3] - enemy_position).y * (obj_position - enemy_position).x ) < 0 &&
                        ( ( enemy_apex[0] - enemy_position ).x * (obj_position - enemy_position).y ) - ((enemy_apex[0] - enemy_position).y * (obj_position - enemy_position).x ) > 0 )
                    {
                        vect = (enemy_apex[3] - enemy_apex[0]);
                        break;
                    }
                }
            }
        }
        return vect;
    }
    //=============================================================//
    //=============================================================//
    /**
     * @brief 0度の時の頂点を計算（現在は四角形のみ）
     * @param Vector3 position
     * @param Vector3 scale
     * @return Vector3[]
    */
    private Vector3[] CalculateApex(Vector3 obj_position, Vector3 obj_scale)
    {
        Vector3[] def = new Vector3[4];
        def[0] = new Vector3(obj_position.x - (obj_scale.x / 2f), obj_position.y + (obj_scale.y / 2f));
        def[1] = def[0] + new Vector3( obj_scale.x, 0 );
        def[2] = def[1] - new Vector3( 0, obj_scale.y );
        def[3] = def[2] - new Vector3( obj_scale.x, 0 );

        return def;
    }
    //=============================================================//

    //=============================================================//
    /**
     * @brief 0度の時の角度を計算（現在は四角形のみ）
     * @param Vector3 position
     * @param List<Vector3> def
     * @return float[]
    */
    private float[] CalculateAngle(Vector3 obj_position, Vector3[] def)
    {
        float[] angle = new float[4];
        angle[0] = Mathf.Atan2( ( def[0] - obj_position ).y, ( def[0] - obj_position ).x ) * Mathf.Rad2Deg; 
        angle[1] = Mathf.Atan2( ( def[1] - obj_position ).y, ( def[1] - obj_position ).x ) * Mathf.Rad2Deg;
        angle[2] = Mathf.Atan2( ( def[2] - obj_position ).y, ( def[2] - obj_position ).x ) * Mathf.Rad2Deg;
        angle[3] = Mathf.Atan2( ( def[3] - obj_position ).y, ( def[3] - obj_position ).x ) * Mathf.Rad2Deg;

        return angle;
    }
    //=============================================================//

    //=============================================================//
    /**
     * @brief 0度の距離を計算（現在は四角形のみ）
     * @param Vector3 position
     * @param List<Vector3> def
     * @return float[]
    */
    private float[] CalculateDistance(Vector3 obj_position, Vector3[] def)
    {
        float[] dis = new float[4];
        dis[0] = Mathf.Sqrt(Mathf.Pow( ( def[0] - obj_position ).x, 2 ) + Mathf.Pow( ( def[0] - obj_position ).y, 2 ));
        dis[1] = Mathf.Sqrt(Mathf.Pow( ( def[1] - obj_position ).x, 2 ) + Mathf.Pow( ( def[1] - obj_position ).y, 2 ));
        dis[2] = Mathf.Sqrt(Mathf.Pow( ( def[2] - obj_position ).x, 2 ) + Mathf.Pow( ( def[2] - obj_position ).y, 2 ));
        dis[3] = Mathf.Sqrt(Mathf.Pow( ( def[3] - obj_position ).x, 2 ) + Mathf.Pow( ( def[3] - obj_position ).y, 2 ));

        return dis;
    }
    //=============================================================//

    //=============================================================//
    /**
     * @brief 0度の距離を計算（現在は四角形のみ）
     * @param float angleZ
     * @param List<float> angle
     * @param List<float> dis
     * @return Vector3[] addVec
    */
    private Vector3[] CalculateAddVector(float angleZ, float[] angle, float[] dis)
    {
        Vector3[] addVec = new Vector3[4]; 
        addVec[0] = new Vector3(dis[0] * Mathf.Cos( ( angle[0] + angleZ ) * Mathf.Deg2Rad ), dis[0] * Mathf.Sin( ( angle[0] + angleZ ) * Mathf.Deg2Rad ) );
        addVec[1] = new Vector3(dis[1] * Mathf.Cos( ( angle[1] + angleZ ) * Mathf.Deg2Rad ), dis[1] * Mathf.Sin( ( angle[1] + angleZ ) * Mathf.Deg2Rad ) ); 
        addVec[2] = new Vector3(dis[2] * Mathf.Cos( ( angle[2] + angleZ ) * Mathf.Deg2Rad ), dis[2] * Mathf.Sin( ( angle[2] + angleZ ) * Mathf.Deg2Rad ) ); 
        addVec[3] = new Vector3(dis[3] * Mathf.Cos( ( angle[3] + angleZ ) * Mathf.Deg2Rad ), dis[3] * Mathf.Sin( ( angle[3] + angleZ ) * Mathf.Deg2Rad ) ); 

        return addVec;
    }
    //=============================================================//
}
//=============================================================//
