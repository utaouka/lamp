﻿using UnityEngine;
using System.Collections;

/**
 * @brief (UI座標用)タッチマネージャー
 * @author 大阪電気通信大学総合情報学部デジタルゲーム学科　大野哲平
 * @since 2014.05.06
 * @version 1.0.0.
*/
public class TouchManager : MonoBehaviour
{
	#region 定数
	//=============================================================//
	/**
	 * @brief 移動方向用の定数
	*/
	public enum MOVE_DIRECTION
	{
		NEUTRAL = 0,	//!< ニュートラル
		RIGHT,			//!< 右
		LEFT,			//!< 左
		UP,				//!< 上
		DOWN,			//!< 下
		MAX,			//!< Max
	}
	/**
	 * @biref 判定する指の数
	*/
	private const int MAX_COUNT = 2;
	/**
	 * @brief タッチ移動認識範囲のニュートラルスペース
	*/
	private static Vector3 NEUTRAL_SPACE = new Vector3 (0.1f, 0.1f);
	//=============================================================//
	#endregion

	#region メンバー変数
	//=============================================================//
	/** 
	 * @brief 自身のインスタンス
	*/
	private static TouchManager instance = null;
	/**
	 * @brief カメラ
	*/
	private static Camera myCamera;
	/**
	 * @brief タッチID
	*/
	private static int[] m_touch_id = new int[MAX_COUNT];
	/**
	 * @brief タッチ座標
	*/
	private static Vector2[] m_touch_pos = new Vector2[MAX_COUNT];
	/**
	 * @brief 移動後のタッチ座標
	*/
	private static Vector2[] m_movetouch_pos = new Vector2[MAX_COUNT];
	/**
	 * @brief 特定範囲内のIDを記憶する
	*/
	private static int m_memory_id = -1;
	//=============================================================//
	#endregion

	//=============================================================//
	/**
	 * @brief 起動時一回のみ起動
	 * @param void
	 * @return void
	*/
	void Awake ()
	{
		// TouchManagerの唯一のインスタンスを生成
		if (instance == null) {
			instance = this;
		} else {
			Destroy (gameObject);
		}
	}
	//=============================================================//

	//=============================================================//
	/**
	 * @brief 初期化
	 * @param void
	 * @return void
	*/
	void Start ()
	{
		for (int i = 0; i < MAX_COUNT; i++) 
		{
			m_touch_pos [i] = new Vector3 ( -2, -2);
			m_movetouch_pos [i] = new Vector3 ( -2, -2);
			m_touch_id [i] = -1;
		}
		myCamera = Camera.main;
	}
	//=============================================================//

	//=============================================================//
	/**
	 * @brief 解放処理
	 * @param void
	 * @return void
	*/
	void OnApplicationQuit()
	{
		for (int i = 0; i < MAX_COUNT; i++) 
		{
			m_touch_pos [i] = new Vector2 ( -2, -2);
			m_movetouch_pos [i] = new Vector2 ( -2, -2);
			m_touch_id [i] = -1;
		}
		instance = null;
		Destroy (gameObject);
	}
	//=============================================================//

	//=============================================================//
	/**
	 * @brief 常時実行処理
	 * @param void
	 * @return void
	*/
	public static void Execution()
	{
		//タッチ判定処理
		if ( Input.touchCount == 0 ) 
		{
			for (int i = 0; i < MAX_COUNT; i++) 
			{
				m_touch_pos [i] = new Vector2 ( -2, -2);
				m_movetouch_pos [i] = new Vector2 ( -2, -2);
				m_touch_id [i] = -1;
			}
		}
		if ( Input.touchCount <= MAX_COUNT )
		{
			for ( int i = 0; i < Input.touchCount; i++ ) 
			{
				Touch t = Input.GetTouch (i);
				if ( m_touch_id [i] != t.fingerId ) 
				{
					m_touch_id [i] = t.fingerId;
				}
				if ( t.phase == TouchPhase.Began ) 
				{
					m_touch_pos [i] = myCamera.ScreenToWorldPoint(t.position);
					m_movetouch_pos [i] = myCamera.ScreenToWorldPoint(t.position);      
				}
				if ( t.phase == TouchPhase.Moved ) 
				{
					m_movetouch_pos [i] = myCamera.ScreenToWorldPoint(t.position);      
				}
				if ( t.phase == TouchPhase.Ended 
					|| t.phase == TouchPhase.Canceled ) 
				{             
					m_touch_pos [i] = new Vector2 ( -2, -2);
					m_movetouch_pos [i] = new Vector2 ( -2, -2);
					m_touch_id [i] = -1;       
				}
			}
		}
	}
	//=============================================================//

	//=============================================================//
	/**
	 * @brief ポーズ時常時実行処理
	 * @param void
	 * @return void
	*/
	public static void PauseExecution()
	{

	}
	//=============================================================//

	//=============================================================//
	/**
	 * @brief どの方向に動いたか？
	 * @param int id (id != -1)
	 * @return int
	*/
	public static int isMove(int id)
	{

		int memory_num = -1;
		for (int i = 0; i < MAX_COUNT; i++) 
		{
			if (id == m_touch_id [i]) 
			{
				memory_num = i;
			}
		}
		if (memory_num == -1) 
		{
			return (int)MOVE_DIRECTION.NEUTRAL;
		}
		if (m_touch_pos [memory_num].x + NEUTRAL_SPACE.x < m_movetouch_pos [memory_num].x) 
		{
			return (int)MOVE_DIRECTION.RIGHT;
		}
		if (m_touch_pos [memory_num].x - NEUTRAL_SPACE.x > m_movetouch_pos [memory_num].x) 
		{
			return (int)MOVE_DIRECTION.LEFT;
		}
		if (m_touch_pos [memory_num].y + NEUTRAL_SPACE.y < m_movetouch_pos [memory_num].y) 
		{
			return (int)MOVE_DIRECTION.UP;
		}
		if (m_touch_pos [memory_num].y - NEUTRAL_SPACE.y > m_movetouch_pos [memory_num].y) 
		{
			return (int)MOVE_DIRECTION.DOWN;
		}
		return (int)MOVE_DIRECTION.NEUTRAL;
	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief 範囲内の指のタッチIDを取得(一つだけ)
	 * @param Vector3 タッチ範囲の左上の座標
	 * @param Vector3 タッチ範囲の右下の座標
	 * @return int TouchID (ID = -1の時は範囲内にタッチなし)
	*/
	public static int GetTouchFingerId(Vector3 XY1, Vector3 XY2)
	{
		//不正チェック
		if (XY1.x > XY2.x || XY1.y < XY2.y) 
		{
			AMDebug.Log("Error XY1 or XY2 ");
			return -1;
		}
		for ( int i = 0; i < MAX_COUNT; i++ ) 
		{
			if ((XY1.x < m_touch_pos [i].x && m_touch_pos [i].x < XY2.x) &&
			    (XY2.y < m_touch_pos [i].y && m_touch_pos [i].y < XY1.y)) 
			{
				m_memory_id = m_touch_id [i];
				break;
			} 
			else
			{
				m_memory_id = -1;
			}
		}
		return m_memory_id;
	}
	//=============================================================//

	//=============================================================//
	/**
	 * @brief タッチ座標を取得
	 * @param int id (id != -1)
	 * @return Vector3 
	*/
	public Vector3 GetTouchPos(int id)
	{
		//不正チェック
		if ( id == -1 ) 
		{
			AMDebug.Log ( "Error TouchID = -1" );
			return Vector3.zero;
		}

		int pos_num = -1;
		for ( int i = 0; i < MAX_COUNT; i++ ) 
		{
			if ( id == m_touch_id [i] ) 
			{
				pos_num = i;
			}
		}

		//不正チェック
		if (pos_num == -1) 
		{
			return Vector3.zero;
		}

		return m_touch_pos[pos_num];
	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief Moveタッチ座標を取得
	 * @param int id (id != -1)
	 * @return Vector3 
	*/
	public Vector3 GetMoveTouchPos(int id)
	{
		//不正チェック
		if ( id == -1 ) 
		{
			AMDebug.Log ( "Error TouchID = -1" );
			return Vector3.zero;
		}

		int move_pos_num = -1;
		for ( int i = 0; i < MAX_COUNT; i++ ) 
		{
			if ( id == m_touch_id [i] ) 
			{
				move_pos_num = i;
			}
		}

		//不正チェック
		if (move_pos_num == -1) 
		{
			return Vector3.zero;
		}

		return m_movetouch_pos[move_pos_num];
	}
	//=============================================================//
	
}
