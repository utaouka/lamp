﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/**
 * @brief プレイヤー追跡用カメラスクロール処理
 * @author 大阪電気通信大学総合情報学部デジタルゲーム学科　大野哲平
 * @since 2014.05.07
 * @version 1.0.0.
*/
public class AppModuleCameraScroll : MonoBehaviour {
	#region 定数
	//=============================================================//

	//=============================================================//
	#endregion

	#region メンバー変数
	//=============================================================//
	/** 
	 * @brief キャラクターのインスタンス
	*/
	private GameObject m_player;
	/**
	 * @brief キャラクターのTransfromインスタンス
	*/
	private Transform m_player_transform;
	/**
	 * @brief スクロールさせるカメラのリスト
	*/
	private static List<Camera> m_camera_list = new List<Camera>();
	/**
	 * @brief スクロールさせるカメラのTransformリスト
	*/
	private static List<Transform> m_camera_transform_list = new List<Transform>();
	/**
	 * @brief スクロールカメラから除去するカメラのリスト
	*/
	private static List<Camera> m_remove_camera_list = new List<Camera>();
	//=============================================================//
	#endregion

	//=============================================================//
	/**
	 * @brief 初期化
	*/
	void Awake() {
		m_camera_list.AddRange (Camera.allCameras);
		for(int i = 0; i < m_camera_list.Count; i++)
		{
			m_camera_transform_list.Add (m_camera_list [i].transform);
		}
	}
	//=============================================================//

	//=============================================================//
	/**
	 * @brief キャラクターのオブジェクト探知
	 * @param void
	 * @return void
	*/
	private void GetPlayerInstance()
	{
		if (GameObject.Find ("Player") != null) 
		{
			m_player = GameObject.Find ("Player");
			m_player_transform = m_player.transform;
		}
	}
	//=============================================================//

	//=============================================================//
	/**
	 * @brief 開始前一回のみ実行
	 * @param void
	 * @return void
	*/
	void Start () {
		AddRemoveScrollCameraList (GameObject.Find ("ViewPortCamera").GetComponent<Camera> ());
		AddRemoveScrollCameraList (GameObject.Find ("UICamera").GetComponent<Camera> ());
	}
	//=============================================================//
	
	//=============================================================//
	/**
	 * @brief 常時実行処理
	 * @param void
	 * @return void
	*/
	void Update () {
		if (m_remove_camera_list.Count > 0) 
		{
			for(int i = 0; i < m_remove_camera_list.Count; i++)
			{
				RemoveScrollCamera (m_remove_camera_list [i]);
			}
		}
		if (m_player == null) 
		{
			GetPlayerInstance ();
			return;
		}
		if (m_player_transform.localPosition.x >= 0) 
		{
			for (int i = 0; i < m_camera_transform_list.Count; i++) 
			{
				m_camera_transform_list [i].localPosition = new Vector3 (m_player_transform.localPosition.x, m_camera_transform_list [i].localPosition.y);
			}
		}
		if (m_player_transform.localPosition.y >= 0) 
		{
			for (int i = 0; i < m_camera_transform_list.Count; i++) 
			{
				m_camera_transform_list [i].localPosition = new Vector3 (m_camera_transform_list [i].localPosition.x, m_player_transform.localPosition.y);
			}
		}
	}
	//=============================================================//

	//=============================================================//
	/**
	 * @brief スクロールするカメラのリストから除去
	 * @param Camera camera
	 * @return void
	*/
	private void RemoveScrollCamera(Camera camera)
	{
		int num = m_camera_list.IndexOf (camera);
		m_camera_list.RemoveAt (num);
		m_camera_transform_list.RemoveAt (num);
		m_remove_camera_list.Remove (camera);
	}
	//=============================================================//
	
	//=============================================================//
	/**
	 * @brief スクロールするカメラのリストから除去するリストに登録
	 * @param Camera camera
	 * @return void
	*/
	public static void AddRemoveScrollCameraList(Camera camera)
	{
		m_remove_camera_list.Add (camera);
	}
	//=============================================================//
}
