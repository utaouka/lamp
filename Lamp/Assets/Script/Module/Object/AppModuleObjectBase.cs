﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/**
 * @brief ゲームオブジェクトの基底クラス
 * @author 大阪電気通信大学総合情報学部デジタルゲーム学科　大野哲平
 * @since 2014.05.07
 * @version 1.0.0.
*/
public class AppModuleObjectBase : ObjectTaskController {

	#region メンバー変数
	//=============================================================//
	/**	
	 * @brief ゲームオブジェクトのインスタンス
	*/
	protected GameObject m_object;
	/**		
	 * @brief スプライトのTransformインスタンス
	*/
	protected Transform m_object_transform;
	/**		
	 * @brief パネルのインスタンス
	*/
	protected GameObject m_panel;
	//=============================================================//
	#endregion

	//=============================================================//
	/**			
	 * @brief コンストラクタ
	*/
	public AppModuleObjectBase()
	{
		//ゲームオブジェクトの生成
		m_object = new GameObject ();
		m_object_transform = m_object.transform;
	}
	//=============================================================//

	//=============================================================//
	/**			
	 * @brief 終了処理
	*/
	public override void Terminate()
	{
		if (m_object != null) 
		{
			UnityEngine.MonoBehaviour.Destroy (m_object);
			m_object = null;
		}
	}
	//=============================================================//

	//=============================================================//
	/**			
	 * @brief 解放処理
	*/
	public virtual void Remove()
	{
		if (m_object != null) 
		{
			UnityEngine.MonoBehaviour.Destroy (m_object);
			m_object = null;
		}
	}
	//=============================================================//

	//=============================================================//
	/**				
	 * @brief オブジェクトのWorld座標取得
	 * @param void
	 * @return Vector3
	*/
	public Vector3 GetPosition()
	{
		return (m_object_transform != null) ? m_object_transform.position : Vector3.zero;
	}
	//=============================================================//

	//=============================================================//
	/*					
	 * @brief オブジェクトのlocal座標取得
	 * @param void
	 * @return Vector3
	*/
	public Vector3 GetlocalPosition()
	{
		return (m_object_transform != null) ? m_object_transform.localPosition : Vector3.zero;
	}
	//=============================================================//

	//=============================================================//
	/**					
	 * @brief オブジェクトのScale取得
	 * @param void
	 * @return Vector3
	*/
	public Vector3 GetScale()
	{
		return (m_object_transform != null) ? m_object_transform.localScale : Vector3.one;
	}
	//=============================================================//

	//=============================================================//
	/**						
	 * @brief オブジェクトの回転角度を取得
	 * @param void
	 * @return Vector3
	*/
	public Vector3 GetEulerAngle()
	{
		return (m_object_transform != null) ? m_object_transform.eulerAngles : Vector3.zero;
	}
	//=============================================================//

    //=============================================================//
    /**                     
     * @brief オブジェクトを取得
     * @param void
     * @return GameObject
    */
    public GameObject GetObject()
    {
        return m_object;
    }
    //=============================================================//

    //=============================================================//
    /**                     
     * @brief オブジェクトのTransformを取得
     * @param void
     * @return Transform
    */
    public Transform GetTransform()
    {
        return m_object_transform;
    }
    //=============================================================//
}

