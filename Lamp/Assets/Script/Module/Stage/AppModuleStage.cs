﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/**
 * @brief ステージ情報保管クラス
*/
public class AppModuleStage {

	#region 定数
	//=============================================================//
	public enum STAGE_TYPE
	{
		MAX,				//!< Max.
	}
	//=============================================================//
	#endregion

	#region メンバー変数
	//=============================================================//
	/**
	 * @brief ゲームオブジェクトのインスタンス
	*/
	private GameObject m_object;
	//=============================================================//
	#endregion	
}
