﻿using UnityEngine;
using System.Collections;
//=============================================================//
/**
 * @brief Sprite描画用クラス
 * @author 大阪電気通信大学総合情報学部デジタルゲーム学科
 * HW11A020 大野哲平.
 * @since2014.04.26
*/
public class AppModuleSprite {

	#region メンバー変数
	//=============================================================//
	//=============================================================//
	#endregion

	//=============================================================//
	/**
	 * @brief スプライトの作成
	 * @param GameObject 親オブジェクト
	 * @param GameObject 子オブジェクト
	 * @param String AtlasPath
	 * @return GameObject
	*/
	public static void CreateSprite( GameObject parent_obj, GameObject child, string AtlasPath )
	{
		UISprite ui_sprite;

		child.transform.parent = parent_obj.transform;
		child.layer = parent_obj.layer;
		child.AddComponent<UISprite> ();
		ui_sprite = child.GetComponent<UISprite> ();
		ui_sprite.atlas = Resources.Load (AtlasPath, typeof(UIAtlas)) as UIAtlas;
	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief スプライトのサイズ取得
	 * @param GameObject obj
	 * @return Vector3
	*/
	public static Vector3 GetSpriteSize( GameObject obj )
	{
		UISprite ui_sprite;

		ui_sprite = obj.GetComponent<UISprite> ();
		return new Vector3 ( ui_sprite.atlas.GetSprite (ui_sprite.spriteName).outer.width, ui_sprite.atlas.GetSprite (ui_sprite.spriteName).outer.height );
	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief スプライトのTypeを設定
	 * @param UISprite.Type
	 * @param GameObject obj
	 * @return void
	*/
	public static void SetSpriteType(UISprite.Type type, GameObject obj)
	{
		UISprite ui_sprite;

		ui_sprite = obj.GetComponent<UISprite> ();
		ui_sprite.type = type;
	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief スプライトのα値を設定
	 * @param float
	 * @param GameObject obj
	 * @return void
	*/
	public static void SetColorAlpha(float a, GameObject obj)
	{
		UISprite ui_sprite;

		ui_sprite = obj.GetComponent<UISprite> ();
		ui_sprite.alpha = a;
	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief スプライト名を設定
	 * @param string
	 * @return void
	*/
	public static void SetSpriteName(string str, GameObject obj)
	{
		UISprite ui_sprite;

		ui_sprite = obj.GetComponent<UISprite> ();
		ui_sprite.spriteName = str;
	}
	//=============================================================//
}
//=============================================================//