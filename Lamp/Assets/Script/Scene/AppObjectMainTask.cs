﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//=============================================================//
/**
 * @brief オブジェクト管理用メインタスク
 * @since 2014.04.26
 * @author 大阪電気通信大学総合情報学部デジタルゲーム学科
 * 		   HW11A020 大野哲平.
*/
public class AppObjectMainTask {

	#region メンバー変数
	//=============================================================//
	/**
	 * @brief オブジェクトタスクのリスト
	*/
	private static List<ManageObjectTask> ObjectTask = new List<ManageObjectTask>();
	/**
	 * @brief 削除するオブジェクトタスクのリスト
	*/
	private static List<ManageObjectTask> RemoveObjectTaskList = new List<ManageObjectTask>();
	/**
	 * @brief 追加するオブジェクトタスクのリスト
	*/
	private static List<ManageObjectTask> AddObjectTaskList = new List<ManageObjectTask>();
	//=============================================================//
	#endregion
	
	//=============================================================//
	/**
	 * @brief オブジェクトタスクをリストに追加(本登録)
	*/
	public static void AddObjectTask()
	{
		if (AddObjectTaskList.Count != 0) 
		{
			ObjectTask.AddRange (AddObjectTaskList);
			SortObjectTask ();
			AddObjectTaskList.Clear ();
		}
	}
	//=============================================================//

	//=============================================================//
	/**
	 * @brief オブジェクトタスクを並び替え
	*/
	public static void SortObjectTask()
	{
		if (ObjectTask.Count == 0) 
		{
			AppMain.NowLoadingObjectTask = null;
			return;
		}
		ObjectTask.Sort((a, b) => a.ID - b.ID);
		AppMain.NowLoadingObjectTask = ObjectTask[0];
	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief オブジェクトタスクの切り替え
	*/
	public static void ChangeObjectTask( ManageObjectTask task )
	{
		if (ObjectTask.Count == 0) 
		{
			AppMain.NowLoadingObjectTask = null;
			return;
		}
		int id = ObjectTask.IndexOf (task);
		if ( id == ObjectTask.Count - 1 )
		{
			AppMain.NowLoadingObjectTask = ObjectTask[0];
		}
		else 
		{
			AppMain.NowLoadingObjectTask = ObjectTask [id + 1];
		}
	}
	//=============================================================//

	//=============================================================//
	/**
	 * @brief オブジェクトタスクの個数を取得
	*/
	public static int GetObjectTaskCount()
	{
		return ObjectTask.Count;
	}
	//=============================================================//

	//=============================================================//
	/**
	 * @brief オブジェクトタスクリストの全削除
	*/
	public static void TaskClearAll()
	{
		ObjectTask.Clear ();
		RemoveObjectTaskList.Clear ();
		AddObjectTaskList.Clear ();
	}
	//=============================================================//

	//=============================================================//
	/**
	 * @brief オブジェクトタスクの削除予約
	 * @param ObjectTaskController
	 * @return bool
	*/
	public static bool AddRemoveTaskList(ObjectTaskController task)
	{
		RemoveObjectTaskList.Add (ObjectTask.Find(x => x.OBJ_TASK == task));
		return true;
	}
	//=============================================================//

	//=============================================================//
	/**
	 * @brief オブジェクトタスクを登録(仮登録)
	*/
	public static void Regist( int id, ObjectTaskController task )
	{
		AddObjectTaskList.Add (new ManageObjectTask (id, task));
	}
	//=============================================================//

	//=============================================================//
	/**
	 * @brief オブジェクトタスクの削除
	*/
	public static void RemoveTask()
	{
		for (int i = 0; i < RemoveObjectTaskList.Count; i++) 
		{
			ObjectTask.Remove (RemoveObjectTaskList[i]);
		}
		RemoveObjectTaskList.Clear ();
	}
	//=============================================================//

}

/**
 * @brief オブジェクトタスクのリスト登録用クラス
*/
public class ManageObjectTask
{
	#region メンバー変数
	//=============================================================//
	/**	
	 * @brief オブジェクトタスクのID
	*/
	public int ID;
	/**	
	 * @brief オブジェクト
	*/
	public ObjectTaskController OBJ_TASK;

	//=============================================================//
	#endregion

	//=============================================================//
	/**
	 * @brief コンストラクタ
	*/
	public ManageObjectTask ( int id, ObjectTaskController task )
	{
		ID = id;
		OBJ_TASK = task;
	}
	//=============================================================//
}

/**
 * @brief オブジェクトタスク用テンプレート
*/
public class ObjectTaskController
{
	//=============================================================//
	/**	
	 * @brief 実行前に一度だけ通る処理
	 * @brief 実際の処理は継承先でオーバーライドして記述
	 * @param void
	 * @return void
	*/
	public virtual void Initialize() 
	{
		//Do Nothing.
	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief 実行処理
	 * @brief 実際の処理は継承先でオーバーライドして記述
	 * @param void
	 * @return void
	*/
	public virtual void Execution ()
	{
		//Do Nothing.
	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief ポーズ中実行処理
	 * @brief 実際の処理は継承先でオーバーライドして記述
	 * @param void
	 * @return void
	*/
	public virtual void PauseExecution ()
	{
		//Do Nothing.
	}
	//=============================================================//

	//=============================================================//
	/**		
	 * @brief 終了処理
	 * @brief 実際の処理は継承先でオーバーライドして記述
	 * @param void
	 * @return void
	*/
	public virtual void Terminate ()
	{

	}
	//=============================================================//
	
}
