﻿using UnityEngine;
using System.Collections;
//=============================================================//
/**
 * @brief シーン管理用メインタスク
 * @since 2014.04.26
 * @author 大阪電気通信大学総合情報学部デジタルゲーム学科
 * 		   HW11A020 大野哲平.
*/
public class AppSceneMainTask : AppMain {

	#region メンバー変数
	//=============================================================//
	/**
	 * @brief シーンタスクのインスタンス
	*/
	public static AppSceneMainTask SceneTask;
	//=============================================================//
	#endregion

	//=============================================================//
	/**
	 * @brief AppMainに現在のシーンを登録
	 * @param void
	 * @return void
	*/
	public override void Start()
	{
		AppMain.AppSceneMainTask = SceneTask;
		base.Start ();
	}
	//=============================================================//

	//=============================================================//
	/**
	 * @brief 実行前に一度だけ通る処理
	 * @brief 実際の処理は継承先でオーバーライドして記述
	 * @param void
	 * @return void
	*/
	public virtual void Initialize() 
	{
		//Do Nothing.
	}
	//=============================================================//

	//=============================================================//
	/**
	 * @brief 実行処理
	 * @brief 実際の処理は継承先でオーバーライドして記述
	 * @param void
	 * @return void
	*/
	public virtual void Execution ()
	{
		//Do Nothing.
	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief ポーズ中実行処理
	 * @brief 実際の処理は継承先でオーバーライドして記述
	 * @param void
	 * @return void
	*/
	public virtual void PauseExecution ()
	{
		//Do Nothing.
	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief 終了処理
	 * @brief 実際の処理は継承先でオーバーライドして記述
	 * @param void
	 * @return void
	*/
	public virtual void Terminate ()
	{
		//Do Nothing.
	}
	//=============================================================//
}
