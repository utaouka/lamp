using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/**
 * @brief プレイヤーコントローラーオブジェクトタスク
*/
public class AppScenePlayScenePlayerController : AppModuleObjectBase {

	#region 定数
	//=============================================================//
	/**
	 * @brief キャラクターの状態
	*/
	public enum CHARA_STATE
	{
		NEUTRAL = 0,	//!< 通常.
        MUTEKI,         //!< 無敵.
		DEAD,			//!< 死亡.
		MAX,			//!< 最大.
	}
	/**
	 * @brief キャラクターの描画状態
	*/
	public enum CHARA_DRAW_STATE
	{
		NEUTRAL = 0,	//!< 通常.
		WALK_RIGHT,		//!< 右に歩く.
		WALK_LEFT,		//!< 左に歩く.
		MAX,			//!< 最大.
	}
	/**
	 * @brief キャラクターの行動状態
	*/
	public enum CHARA_MOVE_STATE
	{
		NEUTRAL = 0,	//!< 通常.
		WALK_RIGHT,		//!< 右に歩く.
		WALK_LEFT,		//!< 左に歩く.
		MAX,			//!< 最大.
	}
    /**
     * @brief キャラクターのジャンプ状態
    */
    public enum CHARA_JUMP_STATE
    {
        NEUTRAL = 0,    //!< 通常.
        START,          //!< 上昇.
        NONE,           //!< ジャンプ不可能.
        MAX,            //!< 最大.
    }
    /**
     * @brief 無敵時間(フレーム)
    */
    private const int MUTEKI_TIME_FRAME = 30;
    /**
     * @brief 無敵時間用フレーム(一秒あたりのフレーム)
    */
    private const int MUTEKI_FRAME = 60;
	/**
	 * @brief アニメーション用フレーム(一秒あたりのフレーム)
	*/
	private const int ANIMATION_FRAME = 60;
    /**
     * @brief ジャンプ力
    */
    private readonly Vector3 JUMP_POWER = new Vector3( 0, 30f );
    /**
     * @brief 重力
    */
    private readonly Vector3 GRAVITY = new Vector3( 0, -10f );
	//=============================================================//
	#endregion

	#region メンバー変数
	//=============================================================//
	/**
	 * @brief 状態
	*/
	private CHARA_STATE m_state;
	/**
	 * @brief 描画状態
	*/
	private CHARA_DRAW_STATE m_draw_state;
	/**
	 * @brief 行動状態
	*/
	private CHARA_MOVE_STATE m_move_state;
    /**
     * @brief ジャンプ状態
    */
    private CHARA_JUMP_STATE m_jump_state;
    /**
     * @brief 無敵時間用カウント
    */
    private float m_muteki_count = 0;
    /**
     * @brief プレーヤー吹き飛ばしベクトル
    */
    private Vector3 m_push_vec;
    /**
     * @brief アニメーション用カウント
    */
    private float m_animation_count;
    /**
     * @brief 当たり判定ループフラグ
    */
    private bool m_hit_loop_flag = true;
    /**
     * @brief 体力最大値
    */
    private int m_max_life;
    /**
     * @brief 体力
    */
    private int m_life;
    /**
     * @brief 攻撃力
    */
    private int m_attack_power;
    /**
     * @brief 防御力
    */
    private int m_defense_power;
    /**
     * @brief プレーヤーの前座標(3フレーム)
    */
    private Vector3[] m_pre_pos;
    /**
     * @brief プレイヤーのジャンプ力(減衰)
    */
    private Vector3 m_jump_power;
	//=============================================================//
	#endregion

	//=============================================================//
	/**
	 * @brief コンストラクタ
	 * @brief このオブジェクトの優先順位を登録
	*/
	public AppScenePlayScenePlayerController()
	{
		AppObjectMainTask.Regist( 0, this );
	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief 実行前に一度だけ通る処理
	 * @param void
	 * @return void
	*/
	public override void Initialize()
	{
		//ゲームオブジェクトの名前設定
		m_object.name = "Player";

		//親パネルの取得
		m_panel = GameObject.Find ("PlayerPanel");

		//オブジェクトの生成と描画処理
		AppModuleSprite.CreateSprite ( m_panel, m_object, "TextureAtlas/Game/g_player2_atlas");

        //オブジェクトの情報決定
        m_max_life = 200;
        m_life = m_max_life;
        m_attack_power = 20;
        m_defense_power = 0;
        m_jump_power = Vector3.zero;

		m_object_transform.localScale = AppModuleSprite.GetSpriteSize (m_object) * 5f;
		m_object_transform.localPosition = new Vector3 (-500f, 0);

        m_pre_pos = new Vector3[4];
        for(int index = 0; index < m_pre_pos.Length; index++)
        {
            m_pre_pos[index] = m_object_transform.localPosition;
        }
	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief 実行処理
	 * @param void
	 * @return void
	*/
	public override void Execution()
	{
		m_animation_count += Time.deltaTime * ANIMATION_FRAME;
		if (m_animation_count > 6000f) 
		{
			m_animation_count = 0;
		}
		Draw ();
		Move ();
        ControlState();
        AdjustLife();


        for(int count = 2; count >= 0; count--)
        {
            m_pre_pos[count + 1] = m_pre_pos[count];
        }
        m_pre_pos[0] = m_object_transform.localPosition;
	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief 描画(アニメーション)処理
	 * @param void
	 * @return void
	*/
	private void Draw()
	{
		switch (m_draw_state) 
		{
		case CHARA_DRAW_STATE.NEUTRAL:
			AppModuleSprite.SetSpriteName ("majo_neutral", m_object);
			break;
		case CHARA_DRAW_STATE.WALK_LEFT:
			if ((int)m_animation_count % ANIMATION_FRAME < ANIMATION_FRAME / 4) 
			{
				AppModuleSprite.SetSpriteName ("majo_left1", m_object);
			} 
			else if ((int)m_animation_count % ANIMATION_FRAME < ANIMATION_FRAME / 4 * 2) 
			{
				AppModuleSprite.SetSpriteName ("majo_left2", m_object);
			} 
			else if ((int)m_animation_count % ANIMATION_FRAME < ANIMATION_FRAME / 4 * 3) 
			{
				AppModuleSprite.SetSpriteName ("majo_left3", m_object);
			} 
			else 
			{
				AppModuleSprite.SetSpriteName ("majo_left2", m_object);
			}
			break;
		case CHARA_DRAW_STATE.WALK_RIGHT:
			if ((int)m_animation_count % ANIMATION_FRAME < ANIMATION_FRAME / 4) 
			{
				AppModuleSprite.SetSpriteName ("majo_right1", m_object);
			} 
			else if ((int)m_animation_count % ANIMATION_FRAME < ANIMATION_FRAME / 4 * 2) 
			{
				AppModuleSprite.SetSpriteName ("majo_right2", m_object);
			} 
			else if ((int)m_animation_count % ANIMATION_FRAME < ANIMATION_FRAME / 4 * 3) 
			{
				AppModuleSprite.SetSpriteName ("majo_right3", m_object);
			} 
			else 
			{
				AppModuleSprite.SetSpriteName ("majo_right2", m_object);
			}
			break;
		}
	}
	//=============================================================//

    //=============================================================//
    /** 
     * @brief キャラクターの移動処理
     * @param void
     * @return void
    */
    public void Move()
    {
        Vector3 add_pos;
        
        //重力の加算
        add_pos = GRAVITY;

        //テスト用処理のためインデント無し
        if(m_state != CHARA_STATE.MUTEKI){

            #if UNITY_EDITOR
            if(Input.GetKey(KeyCode.RightArrow))
            {
                m_move_state = CHARA_MOVE_STATE.WALK_RIGHT;
            }
            if(Input.GetKey(KeyCode.LeftArrow))
            {
                m_move_state = CHARA_MOVE_STATE.WALK_LEFT;
            }
            if(Input.GetKey(KeyCode.Space))
            {
                m_jump_state = CHARA_JUMP_STATE.START;
            }
            #endif

            switch (m_move_state) 
            {
            case CHARA_MOVE_STATE.NEUTRAL:
                m_draw_state = CHARA_DRAW_STATE.NEUTRAL;
                break;
            case CHARA_MOVE_STATE.WALK_RIGHT:
                m_draw_state = CHARA_DRAW_STATE.WALK_RIGHT;
                add_pos += new Vector3 (10f, 0);
                break;
            case CHARA_MOVE_STATE.WALK_LEFT:
                m_draw_state = CHARA_DRAW_STATE.WALK_LEFT;
                add_pos += new Vector3 (-10f, 0);
                break;
            default:
                AMDebug.Log ("Error Move() m_move_state ");
                break;
            }

            switch (m_jump_state) 
            {
            case CHARA_JUMP_STATE.NEUTRAL:
                break;
            case CHARA_JUMP_STATE.START:
                m_jump_power = JUMP_POWER;
                add_pos += m_jump_power;
                SetJumpState(CHARA_JUMP_STATE.NONE);
                break;
            case CHARA_JUMP_STATE.NONE:
                if(m_jump_power.y > 0)
                {
                    m_jump_power += GRAVITY;
                }
                else
                {
                    m_jump_power = Vector3.zero;
                }
                add_pos += m_jump_power;
                break;
            default:
                AMDebug.Log ("Error Move() m_jump_state ");
                break;
            }
        }

        //======================================================//
        //敵との接触を調べて、ジャンプステートを切り替える関数を先に挟む(未実装)
        //敵との接触判定
        if(m_state != CHARA_STATE.MUTEKI)
        {
            while(m_hit_loop_flag)
            {
                add_pos = HitCheck_Enemy(add_pos);
            }
            m_hit_loop_flag = true;
        }
        //======================================================//

        //吹き飛ばし処理(移動処理未対応)
        if(m_state != CHARA_STATE.MUTEKI)
        {
            m_push_vec = AppScenePlaySceneTask.GetEnemyManagerInstance().GetPushVector(GetlocalPosition());
        }
        else
        {
            add_pos += m_push_vec;

            m_push_vec *= 0.94f;
        }

        //壁との接触を調べて、ジャンプステートを切り替える関数を先に挟む(未実装)
        //壁との接触判定
        while(m_hit_loop_flag)
        {
            add_pos = HitCheck_Wall(add_pos);
        }
        m_hit_loop_flag = true;
        m_object_transform.localPosition += add_pos;
    }
    //=============================================================//

    //=============================================================//
    /** 
     * @brief キャラクターと敵の接触判定
     * @param Vector3 add_pos
     * @return Vector3 add_pos
    */
    private Vector3 HitCheck_Enemy(Vector3 add_pos) 
    {
        List<Vector3> hit_vect = AppScenePlaySceneTask.GetEnemyManagerInstance().GetHitVector(add_pos);
        if(hit_vect.Count != 0)
        {
            for(int count = 0; count < hit_vect.Count; count++)
            {
                int angle = (int)(Mathf.Atan2( hit_vect[count].y, hit_vect[count].x ) * Mathf.Rad2Deg + 90);
                if(angle > 180)
                {
                    angle = -360 + angle;
                }
                if(angle == 90)
                {
                    add_pos = add_pos + new Vector3( 0, 1 );
                }
                else if(angle == 180 || angle == -180)
                {
                    add_pos = add_pos + new Vector3( -1, 0 );
                }
                else if(angle == -90)
                {
                    add_pos = add_pos + new Vector3( 0, -1 );
                }
                else if(angle == 0)
                {
                    add_pos = add_pos + new Vector3( 1, 0 );
                }
                else
                {
                    add_pos = add_pos + new Vector3(1 * Mathf.Cos(angle * Mathf.Deg2Rad), 1 * Mathf.Sin(angle * Mathf.Deg2Rad));
                }
                //=====================================================//
                //LandingController(add_pos);
                //=====================================================//
            }
            
            return add_pos;
        }
        else
        {
            m_hit_loop_flag = false;
            return add_pos;
        }
    }
    //=============================================================//

    //=============================================================//
    /** 
     * @brief キャラクターと壁の接触判定
     * @param Vector3 add_pos
     * @return Vector3 add_pos
    */
    private Vector3 HitCheck_Wall(Vector3 add_pos) 
    {
        List<Vector3> hit_vect = AppScenePlaySceneTask.GetWallManagerInstance().GetHitVector(add_pos);
        if(hit_vect.Count != 0)
        {
            for(int count = 0; count < hit_vect.Count; count++)
            {
                int angle = (int)(Mathf.Atan2( hit_vect[count].y, hit_vect[count].x ) * Mathf.Rad2Deg + 90);
                Vector3 regist_vect = Vector3.zero;
                if(angle > 180)
                {
                    angle = -360 + angle;
                }
                if(angle == 90)
                {
                    regist_vect = new Vector3( 0, 1 );
                }
                else if(angle == 180 || angle == -180)
                {
                    regist_vect = new Vector3( -1, 0 );
                }
                else if(angle == -90)
                {
                    regist_vect = new Vector3( 0, -1 );
                }
                else if(angle == 0)
                {
                    regist_vect = new Vector3( 1, 0 );
                }
                else
                {
                    regist_vect = new Vector3(1 * Mathf.Cos(angle * Mathf.Deg2Rad), 1 * Mathf.Sin(angle * Mathf.Deg2Rad));
                }
                add_pos = add_pos + regist_vect;
                //=====================================================//
                LandingController(regist_vect);
                //=====================================================//
            }

            return add_pos;
        }
        else
        {
            m_hit_loop_flag = false;
            return add_pos;
        }
    }
    //=============================================================//
    
    //=============================================================//
    /** 
     * @brief キャラクターの移動状態設定
     * @param int
     * @return void
    */
    public void SetMoveState(CHARA_MOVE_STATE state)
    {
        m_move_state = state;
    }
    //=============================================================//

    //=============================================================//
    /** 
     * @brief キャラクターのジャンプ状態設定
     * @param int
     * @return void
    */
    public void SetJumpState(CHARA_JUMP_STATE state)
    {
        m_jump_state = state;
    }
    //=============================================================//

    //=============================================================//
    /** 
     * @brief キャラクターのジャンプ状態取得
     * @param void
     * @return CHARA_JUMP_STATE
    */
    public CHARA_JUMP_STATE GetJumpState()
    {
        return m_jump_state;
    }
    //=============================================================//

    //=============================================================//
    /** 
     * @brief 着地判定
     * @param Vector3 regist_vect
     * @return void
    */
    public void LandingController(Vector3 regist_vect)
    {
        if( GetJumpState() != CHARA_JUMP_STATE.NEUTRAL )
        {
            Vector3 left_vect = new Vector3( -1f, 1f );
            Vector3 right_vect = new Vector3( 1f, 1f );
            //ベクトルの向きから着地かどうか判定する
            if( (( left_vect.x * regist_vect.y ) - (left_vect.y * regist_vect.x ) < 0 ) &&
                (( right_vect.x * regist_vect.y ) - (right_vect.y * regist_vect.x ) > 0 ) )
            {
                SetJumpState(CHARA_JUMP_STATE.NEUTRAL);
            }
        }
    }
    //=============================================================//

    //=============================================================//
    /** 
     * @brief キャラクターの状態管理
     * @param void
     * @return void
    */
    public void ControlState()
    {
        //無敵状態解除用
        if(m_state == CHARA_STATE.MUTEKI)
        {
            m_muteki_count += Time.deltaTime * MUTEKI_FRAME;
            
            if(m_muteki_count >= MUTEKI_TIME_FRAME)
            {
                m_muteki_count = 0;
                m_state = CHARA_STATE.NEUTRAL;
            }
        }
    }
    //=============================================================//

    //=============================================================//
    /** 
     * @brief キャラクターの体力調整
     * @param void
     * @return void
    */
    public void AdjustLife()
    {
        int damage = 0;

        damage += AppScenePlaySceneTask.GetEnemyManagerInstance().GetCalculateAttackPower();

        if(damage != 0)
        {
            m_life -= damage;

            if(m_life > 0)
            {
                m_state = CHARA_STATE.MUTEKI;
            }
            else
            {
                m_state = CHARA_STATE.DEAD;

                //テスト用処理
                m_life = m_max_life;
                m_object_transform.localPosition = new Vector3 (-500f, 0);
            }
        }
    }
    //=============================================================//

    //=============================================================//
    /**         
     * @brief ポーズ中実行処理
     * @param void
     * @return void
    */
    public override void PauseExecution ()
    {
        
    }
    //=============================================================//

	//=============================================================//
	/**		
	 * @brief 終了処理
	 * @param void
	 * @return void
	*/
	public override void Terminate()
	{
		AppObjectMainTask.AddRemoveTaskList (this);
		base.Terminate ();
	}
	//=============================================================//

	//=============================================================//
	/**		
	 * @brief 解放処理
	 * @param void
	 * @return void
	*/
	public override void Remove()
	{
		AppObjectMainTask.AddRemoveTaskList (this);
		base.Remove ();
	}
	//=============================================================//
	
    //=============================================================//
    /**                      
     * @brief 指定フレーム前(最大3フレーム)のオブジェクトのlocal座標取得
     * @param int フレーム
     * @return Vector3
    */
    public Vector3 GetPrelocalPosition(int frame)
    {
        return  m_pre_pos[frame];
    }
    //=============================================================//
}
