﻿using UnityEngine;
using System.Collections;
//=============================================================//
/**
 * @brief プレイシーンの管理タスク
 * @since 2014.04.26
 * @author 大阪電気通信大学総合情報学部デジタルゲーム学科
 * 		   HW11A020 大野哲平.
*/
public class AppScenePlaySceneTask : AppSceneMainTask {
	#region メンバー変数
	//=============================================================//
	/**
	 * @brief プレイヤーコントローラーのインスタンス
	*/
	public static AppScenePlayScenePlayerController m_player_controller;
	/**
	 * @brief エネミーコントローラーのインスタンス
	*/
	public static AppScenePlaySceneEnemyManager m_enemy_controller;
	/**
	 * @brief ウォールコントローラーのインスタンス
	*/
	public static AppScenePlaySceneWallManager m_wall_controller;
	/**
	 * @brief UIコントローラーのインスタンス
	*/
	public static AppScenePlaySceneUIManagerController m_ui_controller;
	//=============================================================//
	#endregion

	//=============================================================//
	/**
	 * @brief 現在のシーンを登録
	 * @param void
	 * @return void
	*/
	public override void Start()
	{
		AppSceneMainTask.SceneTask = this;
		m_player_controller = new AppScenePlayScenePlayerController();
		m_enemy_controller = new AppScenePlaySceneEnemyManager();
		m_wall_controller = new AppScenePlaySceneWallManager();
		m_ui_controller = new AppScenePlaySceneUIManagerController ();
		base.Start ();
	}
	//=============================================================//

	//=============================================================//
	/**
	 * @brief 実行前に一度だけ通る処理
	 * @param void
	 * @return void
	*/
	public override void Initialize()
	{

	}
	//=============================================================//

	//=============================================================//
	/**
	 * @brief 実行処理
	 * @param void
	 * @return void
	*/
	public override void Execution()
	{
	}
	//=============================================================//

	//=============================================================//
	/**		
	 * @brief ポーズ中実行処理
	 * @param void
	 * @return void
	*/
	public override void PauseExecution ()
	{

	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief 終了処理
	 * @param void
	 * @return void
	*/
	public override void Terminate()
	{
		m_player_controller = null;
		m_enemy_controller = null;
		m_ui_controller = null;
	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief 終了処理
	 * @param void
	 * @return void
	*/
	private void Exit()
	{
		if (m_player_controller != null) 
		{
			m_player_controller.Remove ();
			m_player_controller = null;
		}
		if (m_enemy_controller != null) 
		{
			m_enemy_controller.Remove ();
			m_enemy_controller = null;
		}
		if (m_ui_controller != null)
		{
			m_ui_controller.Remove ();
			m_ui_controller = null;
		}
	}
	//=============================================================//

	//=============================================================//
	/**
	 * @brief プレイヤーのインスタンスを取得
	 * @param void
	 * @return AppScenePlayScenePlayerController
	*/
	public static AppScenePlayScenePlayerController GetPlayerInstance()
	{
		return m_player_controller;
	}
	//=============================================================//
	
	//=============================================================//
	/**
	 * @brief ウォールのインスタンスを取得
	 * @param void
	 * @return AppScenePlaySceneWallManagerController
	*/
	public static AppScenePlaySceneWallManager GetWallManagerInstance()
	{
		return m_wall_controller;
	}
	//=============================================================//

    //=============================================================//
    /**
     * @brief エネミーのインスタンスを取得
     * @param void
     * @return AppScenePlaySceneEnemyManagerController
    */
    public static AppScenePlaySceneEnemyManager GetEnemyManagerInstance()
    {
        return m_enemy_controller;
    }
    //=============================================================//
	
}
//=============================================================//
