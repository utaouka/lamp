﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/**
 * @brief UI(Aボタン)コントローラーオブジェクトタスク
*/
public class AppScenePlaySceneUIAButtonController : AppModuleObjectBase {
    
    #region メンバー変数
    //=============================================================//
    /**
     * @brief 親(Root)オブジェクトのTransformインスタンス
    */
    private Transform m_root_transform;
    /**
     * @brief タッチ認識範囲の左上の座標
    */
    private Vector3 m_rect_lefthunds;
    /**
     * @brief タッチ認識範囲の右下の座標
    */
    private Vector3 m_rect_righthunds;
    //=============================================================//
    #endregion
    
    //=============================================================//
    /**         
     * @brief コンストラクタ
     * @brief このオブジェクトの優先順位を登録
     * @param void
     * @return void
    */
    public AppScenePlaySceneUIAButtonController()
    {
        AppObjectMainTask.Regist( 4, this );
    }
    //=============================================================//
    
    //=============================================================//
    /**             
     * @brief 実行前に一度だけ通る処理
     * @param void
     * @return void
    */
    public override void Initialize()
    {
        //ゲームオブジェクトの名前設定
        m_object.name = "AButton";
        
        //親パネルの取得
        m_panel = GameObject.Find ("UIPanel");
        
        //オブジェクトの生成と描画処理
        AppModuleSprite.CreateSprite ( m_panel, m_object, "TextureAtlas/Game/g_ui_atlas");
        m_object_transform.localScale = AppModuleSprite.GetSpriteSize (m_object);
        m_object_transform.localPosition = new Vector3 (-640 + (m_object_transform.localScale.x / 2) + 980,
                                                        -360 + (m_object_transform.localScale.y / 2) + 20 );
        
        //親ルートのTransform取得
        m_root_transform = m_object_transform.root;
        
        //タッチ認識範囲の座標登録
        m_rect_lefthunds = m_root_transform.TransformPoint (new Vector3 (m_object_transform.localPosition.x - (m_object_transform.localScale.x / 2),
                                                                         m_object_transform.localPosition.y + (m_object_transform.localScale.y / 2)));
        m_rect_righthunds = m_root_transform.TransformPoint (new Vector3 (m_object_transform.localPosition.x + (m_object_transform.localScale.x / 2),
                                                                          m_object_transform.localPosition.y - (m_object_transform.localScale.y / 2)));
    }
    //=============================================================//
    
    //=============================================================//
    /**             
     * @brief 実行処理
     * @param void
     * @return void
    */
    public override void Execution()
    {
        int id = TouchManager.GetTouchFingerId ( m_rect_lefthunds, m_rect_righthunds );

        if(id != -1)
        {
            AppScenePlayScenePlayerController player = AppScenePlaySceneTask.GetPlayerInstance();
            if(player.GetJumpState() == AppScenePlayScenePlayerController.CHARA_JUMP_STATE.NEUTRAL)
            {
                player.SetJumpState (AppScenePlayScenePlayerController.CHARA_JUMP_STATE.START);
            }
        }
    }
    //=============================================================//
    
    //=============================================================//
    /**                     
     * @brief ポーズ中実行処理
     * @param void
     * @return void
    */
    public override void PauseExecution ()
    {
        
    }
    //=============================================================//
    
    //=============================================================//
    /**                 
     * @brief 終了処理
     * @param void
     * @return void
    */
    public override void Terminate()
    {
        AppObjectMainTask.AddRemoveTaskList (this);
        base.Terminate ();
    }
    //=============================================================//
    
    //=============================================================//
    /**     
     * @brief 解放処理
     * @param void
     * @return void
    */
    public override void Remove()
    {
        AppObjectMainTask.AddRemoveTaskList (this);
        base.Remove ();
    }
    //=============================================================//
}

