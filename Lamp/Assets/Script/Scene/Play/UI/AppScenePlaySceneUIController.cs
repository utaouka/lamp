﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/*
 * @brief UIコントローラーオブジェクトタスク
*/
public class AppScenePlaySceneUIManagerController : ObjectTaskController {

	#region メンバー変数
	//=============================================================//
	/**
	 * @brief UI(十字キー)コントローラーのインスタンス
	*/
	public static AppScenePlaySceneUICrossKeyController m_ui_crosskey_controller;
    /**
     * @brief UI(Aボタン)コントローラーのインスタンス
    */
    public static AppScenePlaySceneUIAButtonController m_ui_abutton_controller;
	//=============================================================//
	#endregion

	//=============================================================//
	/**		
	 * @brief コンストラクタ
	 * @param void
	 * @return void
	*/
	public AppScenePlaySceneUIManagerController()
	{
		AppObjectMainTask.Regist( 2, this );
		m_ui_crosskey_controller = new AppScenePlaySceneUICrossKeyController ();
        m_ui_abutton_controller = new AppScenePlaySceneUIAButtonController ();
	}
	//=============================================================//

	//=============================================================//
	/**			
	 * @brief 実行前に一度だけ通る処理
	 * @param void
	 * @return void
	*/
	public override void Initialize()
	{
	}
	//=============================================================//

	//=============================================================//
	/**			
	 * @brief 実行処理
	 * @param void
	 * @return void
	*/
	public override void Execution()
	{

	}
	//=============================================================//

	//=============================================================//
	/**					
	 * @brief ポーズ中実行処理
	 * @param void
	 * @return void
	*/
	public override void PauseExecution ()
	{

	}
	//=============================================================//

	//=============================================================//
	/**				
	 * @brief 終了処理
	 * @param void
	 * @return void
	*/
	public override void Terminate()
	{
		AppObjectMainTask.AddRemoveTaskList (this);
		m_ui_crosskey_controller = null;
        m_ui_abutton_controller = null;
	}
	//=============================================================//

	//=============================================================//
	/**		
	 * @brief 解放処理
	 * @param void
	 * @return void
	*/
	public void Remove()
	{
		AppObjectMainTask.AddRemoveTaskList (this);
		if(m_ui_crosskey_controller != null)
		{
			m_ui_crosskey_controller.Remove ();
			m_ui_crosskey_controller = null;
		}
        if(m_ui_abutton_controller != null)
        {
            m_ui_abutton_controller.Remove ();
            m_ui_abutton_controller = null;
        }
	}
	//=============================================================//

}

