﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//=============================================================//
/**
 * @brief エネミーマネージャーコントローラーオブジェクトタスク
*/
public class AppScenePlaySceneEnemyManager : ObjectTaskController {
    
    #region 定数
    //=============================================================//
    //=============================================================//
    #endregion
    
    #region メンバー変数
    //=============================================================//
    /**
     * @brief 衝突方向の保持
    */
    //private eDirection m_hit_direction;
    /**
     * @brief 敵のリスト
    */
    private List<AppScenePlaySceneEnemyBase> m_enemy_list = new List<AppScenePlaySceneEnemyBase>();
    /**
     * @brief プレイヤーと接触している敵のリスト
    */
    private List<AppScenePlaySceneEnemyBase> m_hit_enemy_list = new List<AppScenePlaySceneEnemyBase>();
    /**
     * @brief ウォールコントローラーのインスタンス
    */
    private AppModuleHitManager m_hit_manager;
    //=============================================================//
    #endregion
    
    //=============================================================//
    /** 
     * @brief コンストラクタ
     * @brief このオブジェクトの優先順位を登録
    */
    public AppScenePlaySceneEnemyManager()
    {
        AppObjectMainTask.Regist (1, this);
        //ヒットマネージャーの作成
        m_hit_manager = new AppModuleHitManager ();
    }
    //=============================================================//
    
    //=============================================================//
    /**     
     * @brief 実行前に一度だけ通る処理
     * @param void
     * @return void
    */
    public override void Initialize()
    {
        m_enemy_list.Add (new AppScenePlaySceneEnemyController (new Vector3 ( -10f, 500f ), new Vector3 ( 160f, 160f ) ) );
    }
    //=============================================================//
    
    //=============================================================//
    /**     
     * @brief 実行処理
     * @param void
     * @return void
    */
    public override void Execution()
    {
    }
    //=============================================================//

    //=============================================================//
    /** 
     * @brief 衝突方向の取得
     * @param Vector3 player_addPos
     * @return List<Vector3>
    */
    public List<Vector3> GetHitVector(Vector3 player_addPos)
    {
        //==============================================//
        List<Vector3> hit_vector = new List<Vector3>();
        for(int index = 0; index < m_enemy_list.Count; index++)
        {
            Vector3 vect = m_hit_manager.CheckHitVector(m_enemy_list[index].GetTransform(), m_enemy_list[index].GetAddPos(), player_addPos);
            if(vect != Vector3.zero)
            {
                m_hit_enemy_list.Add(m_enemy_list[index]);

                hit_vector.Add(vect);
            }
        }
        return hit_vector;
        //==============================================//
    }
    //=============================================================//

    //=============================================================//
    /** 
     * @brief 攻撃力の取得
     * @param void
     * @return int
    */
    public int GetCalculateAttackPower()
    {
        int attack_power = 0;

        for(int index = 0; index < m_hit_enemy_list.Count; index++)
        {
            if(attack_power <= m_hit_enemy_list[index].GetAttackPower())
            {
                attack_power = m_hit_enemy_list[index].GetAttackPower();
            }
        }

        m_hit_enemy_list.Clear();

        return attack_power;
    }
    //=============================================================//

    //=============================================================//
    /** 
     * @brief 吹き飛ばしベクトルの取得
     * @param Vector3 pos
     * @return Vector3 add_pos
    */
    public Vector3 GetPushVector(Vector3 pre_pos)
    {
        Vector3 add_pos;

        if(m_hit_enemy_list.Count == 0)
        {
            add_pos = new Vector3(0, 0);
        }
        else
        {
            if(pre_pos.x < m_hit_enemy_list[0].GetlocalPosition().x)
            {
                add_pos = new Vector3(-30f, 22f);
            }
            else
            {
                add_pos = new Vector3(30f, 22f);
            }
        }

        return add_pos;
    }
    //=============================================================//

    //=============================================================//
    /**             
     * @brief ポーズ中実行処理
     * @param void
     * @return void
    */
    public override void PauseExecution ()
    {
        
    }
    //=============================================================//
    
    //=============================================================//
    /**         
     * @brief 終了処理
     * @param void
     * @return void
    */
    public override void Terminate()
    {
        AppObjectMainTask.AddRemoveTaskList (this);
    }
    //=============================================================//
    
    //=============================================================//
    /**     
     * @brief 解放処理
     * @param void
     * @return void
    */
    public void Remove()
    {
        AppObjectMainTask.AddRemoveTaskList (this);
    }
    //=============================================================//
}

