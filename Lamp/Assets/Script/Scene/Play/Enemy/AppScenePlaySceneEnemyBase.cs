﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//=============================================================//
/**
 * @brief 敵クラスの基底クラス
*/
public class AppScenePlaySceneEnemyBase : AppModuleObjectBase {
    
    #region 定数
    //=============================================================//
    //=============================================================//
    #endregion
    
    #region メンバー変数
    //=============================================================//
    /**
     * @brief キャラクターの最大値
    */
    protected int m_max_life;
    /**
     * @brief キャラクターの体力
    */
    protected int m_life;
    /**
     * @brief キャラクターの攻撃力
    */
    protected int m_attack_power;
    /**
     * @brief キャラクターの防御力
    */
    protected int m_defense_power;
    /**
     * @brief キャラクターの次フレーム移動量
    */
    protected Vector3 m_add_pos;
    //=============================================================//
    #endregion
    
    //=============================================================//
    /**
     * @brief コンストラクタ
    */
    public AppScenePlaySceneEnemyBase()
    {
        
    }
    //=============================================================//

    //=============================================================//
    /** 
     * @brief キャラクターの次フレーム移動量設定
     * @param Vector3 addpos
     * @return void
    */
    protected void SetAddPos(Vector3 add_pos)
    {
        m_add_pos = add_pos;
    }
    //=============================================================//

    //=============================================================//
    /** 
     * @brief キャラクターの次フレーム移動量取得
     * @param void
     * @return Vector3 m_add_pos
    */
    public Vector3 GetAddPos()
    {
        return m_add_pos;
    }
    //=============================================================//

    //=============================================================//
    /** 
     * @brief キャラクターの攻撃力を取得
     * @param  void
     * @return int
    */
    public int GetAttackPower()
    {
        return m_attack_power;
    }
    //=============================================================//
    
    //=============================================================//
    /**     
     * @brief 終了処理
     * @param void
     * @return void
    */
    public override void Terminate()
    {
        base.Terminate ();
    }
    //=============================================================//
    
    //=============================================================//
    /**     
     * @brief 解放処理
     * @param void
     * @return void
    */
    public override void Remove()
    {
        base.Remove ();
    }
    //=============================================================//
    
}

