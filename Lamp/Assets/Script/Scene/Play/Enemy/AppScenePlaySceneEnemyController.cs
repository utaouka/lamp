﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//=============================================================//
/**
 * @brief エネミーコントローラーオブジェクトタスク
*/
public class AppScenePlaySceneEnemyController : AppScenePlaySceneEnemyBase {
    
    #region 定数
    //=============================================================//
    /**
     * @brief キャラクターの状態
    */
    public enum CHARA_STATE
    {
        NEUTRAL = 0,    //!< 通常.
        DEAD,           //!< 死亡.
        MAX,            //!< 最大.
    }
    /**
     * @brief キャラクターの描画状態
    */
    public enum CHARA_DRAW_STATE
    {
        NEUTRAL = 0,    //!< 通常.
        WALK_RIGHT,     //!< 右に歩く.
        WALK_LEFT,      //!< 左に歩く.
        MAX,            //!< 最大.
    }
    /**
     * @brief キャラクターの行動状態
    */
    public enum CHARA_MOVE_STATE
    {
        NEUTRAL = 0,    //!< 通常.
        WALK_RIGHT,     //!< 右に歩く.
        WALK_LEFT,      //!< 左に歩く.
        MAX,            //!< 最大.
    }
    /**
     * @brief ジャンプ時間(フレーム)
    */
    private const int JUMP_TIME_FRAME = 12;
    /**
     * @brief アニメーション用フレーム(一秒あたりのフレーム)
    */
    private const int ANIMATION_FRAME = 60;
    //=============================================================//
    #endregion
    
    #region メンバー変数
    //=============================================================//
    /**
     * @brief 状態
    */
    private CHARA_STATE m_state;
    /**
     * @brief 描画状態
    */
    private CHARA_DRAW_STATE m_draw_state;
    /**
     * @brief 行動状態
    */
    private CHARA_MOVE_STATE m_move_state;
    /**
     * @brief アニメーション用カウント
    */
    private float m_animation_count;
    /**
     * @brief 当たり判定ループフラグ
    */
    private bool m_hit_loop_flag = true;
    //=============================================================//
    #endregion
    
    //=============================================================//
    /**
     * @brief コンストラクタ
     * @brief このオブジェクトの優先順位を登録
     * @param Vector3 座標
     * @param Vector3 スケール
     * @return void
    */
    public AppScenePlaySceneEnemyController( Vector3 position, Vector3 scale )
    {
        AppObjectMainTask.Regist( 2, this );
        
        //ゲームオブジェクトの名前設定
        m_object.name = "Enemy";
        
        //親パネルの取得
        m_panel = GameObject.Find ("EnemyPanel");
        
        //オブジェクトの生成と描画処理
        AppModuleSprite.CreateSprite ( m_panel, m_object, "TextureAtlas/Game/g_player2_atlas");
        //AppModuleSprite.SetSpriteType(UISprite.Type.Tiled, m_object);
        
        //オブジェクトの情報決定
        m_max_life = 100;
        m_life = m_max_life;
        m_attack_power = 20;
        m_defense_power = 0;

        m_object_transform.localScale = scale;
        m_object_transform.localPosition = position;
    }
    //=============================================================//
    
    //=============================================================//
    /** 
     * @brief 実行前に一度だけ通る処理
     * @param void
     * @return void
    */
    public override void Initialize()
    {
        
    }
    //=============================================================//
    
    //=============================================================//
    /** 
     * @brief 実行処理
     * @param void
     * @return void
    */
    public override void Execution()
    {
        m_animation_count += Time.deltaTime * ANIMATION_FRAME;
        if (m_animation_count > 6000f) 
        {
            m_animation_count = 0;
        }
        Draw ();
        Move ();
    }
    //=============================================================//
    
    //=============================================================//
    /** 
     * @brief 描画(アニメーション)処理
     * @param void
     * @return void
    */
    private void Draw()
    {
        switch (m_draw_state) 
        {
        case CHARA_DRAW_STATE.NEUTRAL:
            AppModuleSprite.SetSpriteName ("majo_neutral", m_object);
            break;
        case CHARA_DRAW_STATE.WALK_LEFT:
            if ((int)m_animation_count % ANIMATION_FRAME < ANIMATION_FRAME / 4) 
            {
                AppModuleSprite.SetSpriteName ("majo_left1", m_object);
            } 
            else if ((int)m_animation_count % ANIMATION_FRAME < ANIMATION_FRAME / 4 * 2) 
            {
                AppModuleSprite.SetSpriteName ("majo_left2", m_object);
            } 
            else if ((int)m_animation_count % ANIMATION_FRAME < ANIMATION_FRAME / 4 * 3) 
            {
                AppModuleSprite.SetSpriteName ("majo_left3", m_object);
            } 
            else 
            {
                AppModuleSprite.SetSpriteName ("majo_left2", m_object);
            }
            break;
        case CHARA_DRAW_STATE.WALK_RIGHT:
            if ((int)m_animation_count % ANIMATION_FRAME < ANIMATION_FRAME / 4) 
            {
                AppModuleSprite.SetSpriteName ("majo_right1", m_object);
            } 
            else if ((int)m_animation_count % ANIMATION_FRAME < ANIMATION_FRAME / 4 * 2) 
            {
                AppModuleSprite.SetSpriteName ("majo_right2", m_object);
            } 
            else if ((int)m_animation_count % ANIMATION_FRAME < ANIMATION_FRAME / 4 * 3) 
            {
                AppModuleSprite.SetSpriteName ("majo_right3", m_object);
            } 
            else 
            {
                AppModuleSprite.SetSpriteName ("majo_right2", m_object);
            }
            break;
        }
    }
    //=============================================================//
    
    //=============================================================//
    /** 
     * @brief キャラクターの移動処理
     * @param void
     * @return void
    */
    public void Move()
    {
        Vector3 add_pos;
        
        //重力の加算
        add_pos = new Vector3(0, -10f);

        switch (m_move_state) 
        {
        case CHARA_MOVE_STATE.NEUTRAL:
            m_draw_state = CHARA_DRAW_STATE.NEUTRAL;
            break;
        case CHARA_MOVE_STATE.WALK_RIGHT:
            m_draw_state = CHARA_DRAW_STATE.WALK_RIGHT;
            add_pos += new Vector3 (10f, 0);
            break;
        case CHARA_MOVE_STATE.WALK_LEFT:
            m_draw_state = CHARA_DRAW_STATE.WALK_LEFT;
            add_pos += new Vector3 (-10f, 0);
            break;
        default:
            AMDebug.Log ("Error Move() m_move_state ");
            break;
        }
        
        //壁との接触判定
        while(m_hit_loop_flag)
        {
            add_pos = HitCheck_Wall(add_pos);
        }
        m_hit_loop_flag = true;
        m_object_transform.localPosition += add_pos;
    }
    //=============================================================//

    //=============================================================//
    /** 
     * @brief キャラクターと壁の接触判定
     * @param Vector3 add_pos
     * @return Vector3 add_pos
    */
    private Vector3 HitCheck_Wall(Vector3 add_pos) 
    {
        List<Vector3> hit_vect = AppScenePlaySceneTask.GetWallManagerInstance().GetHitVector2(GetTransform(), add_pos);
        if(hit_vect.Count != 0)
        {
            for(int count = 0; count < hit_vect.Count; count++)
            {
                int angle = (int)(Mathf.Atan2( hit_vect[count].y, hit_vect[count].x ) * Mathf.Rad2Deg + 90);
                if(angle > 180)
                {
                    angle = -180 + (angle - 180);
                }
                if(angle == 90)
                {
                    add_pos = add_pos + new Vector3( 0, 1 );
                }
                else if(angle == 180 || angle == -180)
                {
                    add_pos = add_pos + new Vector3( -1, 0 );
                }
                else if(angle == -90)
                {
                    add_pos = add_pos + new Vector3( 0, -1 );
                }
                else if(angle == 0)
                {
                    add_pos = add_pos + new Vector3( 1, 0 );
                }
                else
                {
                    add_pos = add_pos + new Vector3(1 * Mathf.Cos(angle * Mathf.Deg2Rad), 1 * Mathf.Sin(angle * Mathf.Deg2Rad));
                }
            }
            return add_pos;
        }
        else
        {
            m_hit_loop_flag = false;
            return add_pos;
        }
    }
    //=============================================================//

    //=============================================================//
    /** 
     * @brief キャラクターの移動状態設定
     * @param int
     * @return void
    */
    public void SetMoveState(CHARA_MOVE_STATE state)
    {
        m_move_state = state;
    }
    //=============================================================//

    //=============================================================//
    /**         
     * @brief ポーズ中実行処理
     * @param void
     * @return void
    */
    public override void PauseExecution ()
    {
        
    }
    //=============================================================//

    //=============================================================//
    /**     
     * @brief 終了処理
     * @param void
     * @return void
    */
    public override void Terminate()
    {
        AppObjectMainTask.AddRemoveTaskList (this);
        base.Terminate ();
    }
    //=============================================================//
    
    //=============================================================//
    /**     
     * @brief 解放処理
     * @param void
     * @return void
    */
    public override void Remove()
    {
        AppObjectMainTask.AddRemoveTaskList (this);
        base.Remove ();
    }
    //=============================================================//
    
}
