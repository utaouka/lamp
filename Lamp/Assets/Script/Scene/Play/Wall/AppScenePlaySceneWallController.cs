using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//=============================================================//
/**
 * @brief ウォールコントローラーオブジェクトタスク
*/
public class AppScenePlaySceneWallController : AppScenePlaySceneWallBase {

	#region 定数
	//=============================================================//

	//=============================================================//
	#endregion

	#region メンバー変数
	//=============================================================//
	//=============================================================//
	#endregion

	//=============================================================//
	/**
	 * @brief コンストラクタ
	 * @brief このオブジェクトの優先順位を登録
     * @param Vector3 座標
     * @param Vector3 スケール
     * @return void
	*/
    public AppScenePlaySceneWallController( Vector3 position, Vector3 scale , Vector3 angle )
	{
		AppObjectMainTask.Regist( 5, this );
		
        //ゲームオブジェクトの名前設定
        m_object.name = "Wall";

        //親パネルの取得
        m_panel = GameObject.Find ("WallPanel");

        //オブジェクトの生成と描画処理
        AppModuleSprite.CreateSprite ( m_panel, m_object, "TextureAtlas/Game/g_floor_block_atlas");
        AppModuleSprite.SetSpriteType(UISprite.Type.Tiled, m_object);

        //オブジェクトの情報決定
        m_object_transform.localScale = scale;
        m_object_transform.localPosition = position;
        m_object_transform.eulerAngles = angle;

        m_add_pos = Vector3.zero;
	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief 実行前に一度だけ通る処理
	 * @param void
	 * @return void
	*/
	public override void Initialize()
	{

	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief 実行処理
	 * @param void
	 * @return void
	*/
	public override void Execution()
	{
		Draw ();
		Move ();
	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief 描画(アニメーション)処理
	 * @param void
	 * @return void
	*/
	private void Draw()
	{
		
	}
	//=============================================================//

	//=============================================================//
	/**	
	 * @brief キャラクターの移動処理
	 * @param void
	 * @return void
	*/
    private void Move()
	{
        //コンストラクタ呼び出し時に引数としてadd_posを設定する
        Vector3 add_pos = new Vector3(1,0,0);

        //まず最初に移動させる
        //m_object_transform.localPosition += GetAddPos();
        //m_object_transform.eulerAngles += new Vector3(0,0,1);
        //次フレーム移動量の設定
        SetAddPos(add_pos);
	}
	//=============================================================//

    //=============================================================//
    /**         
     * @brief ポーズ中実行処理
     * @param void
     * @return void
    */
    public override void PauseExecution ()
    {
        
    }
    //=============================================================//

	//=============================================================//
	/**		
	 * @brief 終了処理
	 * @param void
	 * @return void
	*/
	public override void Terminate()
	{
		AppObjectMainTask.AddRemoveTaskList (this);
		base.Terminate ();
	}
	//=============================================================//

	//=============================================================//
	/**		
	 * @brief 解放処理
	 * @param void
	 * @return void
	*/
	public override void Remove()
	{
		AppObjectMainTask.AddRemoveTaskList (this);
		base.Remove ();
	}
	//=============================================================//

}
