﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//=============================================================//
/**
 * @brief ウォールマネージャーオブジェクトタスク
*/
public class AppScenePlaySceneWallManager : ObjectTaskController {

	#region 定数
	//=============================================================//
	//=============================================================//
	#endregion
	
	#region メンバー変数
	//=============================================================//
    /**
     * @brief 壁のリスト
    */
    private List<AppScenePlaySceneWallBase> m_wall_list = new List<AppScenePlaySceneWallBase>();
    /**
     * @brief ウォールコントローラーのインスタンス
    */
    private AppModuleHitManager m_hit_manager;
	//=============================================================//
	#endregion

	//=============================================================//
	/**	
	 * @brief コンストラクタ
	 * @brief このオブジェクトの優先順位を登録
	*/
	public AppScenePlaySceneWallManager()
    {
        AppObjectMainTask.Regist (4, this);
        //ヒットマネージャーの作成
        m_hit_manager = new AppModuleHitManager ();
    }
	//=============================================================//

	//=============================================================//
	/**		
	 * @brief 実行前に一度だけ通る処理
	 * @param void
	 * @return void
	*/
	public override void Initialize()
	{
        //壁の比率は100:1以上にしないこと（原因不明の力で滑るため）
        m_wall_list.Add (new AppScenePlaySceneWallController (new Vector3 ( -500f, -200f ), new Vector3 ( 3000f, 30f ), new Vector3 ( 0, 0, 0 ) ) );
        m_wall_list.Add (new AppScenePlaySceneWallController (new Vector3 ( 150f, -300f ), new Vector3 ( 30f, 600f ), new Vector3 ( 0, 0, 0 ) ) );
        m_wall_list.Add (new AppScenePlaySceneWallController (new Vector3 ( 1000f, -200f ), new Vector3 ( 30f, 600f ), new Vector3 ( 0, 0, 80f ) ) );
        m_wall_list.Add (new AppScenePlaySceneWallController (new Vector3 ( 1500f, -200f ), new Vector3 ( 30f, 600f ), new Vector3 ( 0, 0, -80f ) ) );
    }
	//=============================================================//

	//=============================================================//
	/**		
	 * @brief 実行処理
	 * @param void
	 * @return void
	*/
	public override void Execution()
	{
	}
	//=============================================================//
	
	//=============================================================//
	/**	
	 * @brief 衝突方向の取得
	 * @param Vector3 player_addPos
	 * @return List<Vector3>
	*/
    public List<Vector3> GetHitVector(Vector3 player_addPos)
	{
        //==============================================//
        List<Vector3> hit_vector = new List<Vector3>();
        for(int index = 0; index < m_wall_list.Count; index++)
        {
            Vector3 vect = m_hit_manager.CheckHitVector(m_wall_list[index].GetTransform(), m_wall_list[index].GetAddPos(), player_addPos);
            if(vect != Vector3.zero)
            {
                hit_vector.Add(vect);
            }
        }
        return hit_vector;
        //==============================================//
	}
	//=============================================================//

    //=============================================================//
    /** 
     * @brief 衝突方向の取得
     * @param Vector3 enemy_addPos
     * @return List<Vector3>
    */
    public List<Vector3> GetHitVector2(Transform obj_transform, Vector3 enemy_addPos)
    {
        List<Vector3> hit_vector = new List<Vector3>();
        for(int index = 0; index < m_wall_list.Count; index++)
        {
            Vector3 vect = m_hit_manager.CheckHitVector2(m_wall_list[index].GetTransform(), m_wall_list[index].GetAddPos(), obj_transform, enemy_addPos);
            if(vect != Vector3.zero)
            {
                hit_vector.Add(vect);
            }
        }
        return hit_vector;
    }
    //=============================================================//
	
	//=============================================================//
	/**				
	 * @brief ポーズ中実行処理
	 * @param void
	 * @return void
	*/
	public override void PauseExecution ()
	{

	}
	//=============================================================//

	//=============================================================//
	/**			
	 * @brief 終了処理
	 * @param void
	 * @return void
	*/
	public override void Terminate()
	{
		AppObjectMainTask.AddRemoveTaskList (this);
	}
	//=============================================================//

	//=============================================================//
	/**		
	 * @brief 解放処理
	 * @param void
	 * @return void
	*/
	public void Remove()
	{
		AppObjectMainTask.AddRemoveTaskList (this);
	}
	//=============================================================//
}

